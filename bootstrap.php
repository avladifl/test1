<?php defined('ABSPATH') or exit('');

require_once INCLUDES.'/lib.php';
require_once INCLUDES.'/db.php';
require_once INCLUDES.'/table.php';

require_once INCLUDES.'/table-files.php';
require_once INCLUDES.'/table-categories.php';
require_once INCLUDES.'/ajax.php';
require_once INCLUDES.'/core.php';

$db = new DB(DB_USER, DB_PASSWORD, DB_NAME, DB_HOST);
$ajax = new Ajax();
$core = new Core();

$core->init();

