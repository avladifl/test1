<?php
define('ABSPATH', str_replace('\\', '/', dirname(__FILE__)).'/');

// Инициализация настроек
require_once ABSPATH.'settings.php';

// Загрузка программы
require_once ABSPATH.'bootstrap.php';

