<?php defined('ABSPATH') or exit('');

class Table {
	public $items;
  public $_pagination_args = array();
  public $_actions;
  public $_pagination;

	function __construct() {}
	function prepare_items() {}
	function no_items() { echo 'Элементов не найдено'; }
	function get_bulk_actions() { return array(); }
	function get_columns() {}
	function extra_tablenav( $which ) {}

  function search_box( $text, $name ) {
    $s = isset($_REQUEST[$name]) ? $_REQUEST[$name] : ''; ?>
    <form action="<?php echo SITEURL; ?>">
      <p class="search-box">
        <input type="search" class="widefat search" name="<?php echo $name; ?>" placeholder="Поиск" value="<?php echo $s; ?>" />
        <input type="submit" name="" class="button action search-submit" value="<?php echo $text; ?>">
      </p>
    </form>
  <?php
  }

  function display_tablenav( $which ) { ?>
    <div class="tablenav <?php echo $which; ?>">
      <div class="alignleft actions bulkactions">
        <?php $this->bulk_actions(); ?>
      </div>
      <?php
        $this->extra_tablenav( $which );
        $this->pagination( $which );
      ?>
      <br class="clear" />
    </div>
    <?php
  }

  function display() {
    $this->display_tablenav( 'top' ); ?>
    <table class="list-table widefat">
      <thead>
      <tr>
        <?php $this->print_column_headers(); ?>
      </tr>
      </thead>

      <tfoot>
      <tr>
        <?php $this->print_column_headers( false ); ?>
      </tr>
      </tfoot>

      <tbody class="av-selectable" id="the-list">
      <?php $this->display_rows(); ?>
      </tbody>
    </table>
    <?php
    $this->display_tablenav( 'bottom' );
  }

  function bulk_actions() {
    $two = '';
    if ( is_null( $this->_actions ) ) {
      $this->_actions = $this->get_bulk_actions();
    }
    else {
      $two = '2';
    }

    if ( empty( $this->_actions ) )
      return;

    echo "<select name='action$two'>\n";
    echo "<option value='-1' selected='selected'>Действия</option>\n";

    foreach ( $this->_actions as $name => $title ) {
      echo "\t<option value='$name'>$title</option>\n";
    }

    echo "</select>\n";
    echo "<input type='submit' name='' id='doaction$two' class='button action' value='Применить'>";
  }

  function print_column_headers( $with_id = true ) {
    $columns = $this->get_columns();

    if ( ! empty( $columns['cb'] ) ) {
      static $cb_counter = 1;
      $columns['cb'] = '<input id="cb-select-all-' . $cb_counter . '" type="checkbox" />';
      $cb_counter++;
    }

    foreach ( $columns as $column_key => $column_name ) {
      $class = array( 'manage-column', "column-$column_key" );

      if ( 'cb' == $column_key )
        $class[] = 'check-column';

      $id = $with_id ? "id='$column_key'" : '';
      $class = "class='" . join(' ', $class) . "'";

      echo "<th scope='col' $id $class>$column_name</th>";
    }
  }

  function pagination( $which ) {
    global $pluralElem;
    if ( empty( $this->_pagination_args ) )
      return;

    extract( $this->_pagination_args, EXTR_SKIP );

    $output = '<span class="displaying-num">'.$total_items.' '.$pluralElem[plural( $total_items )].'</span>';

    $current = $this->get_pagenum();

    $current_url = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
    $query_string = ltrim(strstr($current_url, '?'), '?');
    $current_uri = str_replace($query_string, '', $current_url);

    if(mb_strpos($query_string, 'ajax=1') === false)
      $current_uri .= '?ajax=1&';

    $page_links = array();

    $disable_first = $disable_last = '';
    if ( $current == 1 )
      $disable_first = ' disabled';
    if ( $current == $total_pages )
      $disable_last = ' disabled';

    $page_links[] = sprintf( "<a class='%s' title='%s' href='%s'>%s</a>",
      'first-page' . $disable_first,
      'Перейти на первую страницу',
      $current_uri.add_query_arg($query_string, 'paged', false),
      '&laquo;'
    );

    $page_links[] = sprintf( "<a class='%s' title='%s' href='%s'>%s</a>",
      'prev-page' . $disable_first,
      'Перейти на предыдущую страницу',
      $current_uri.add_query_arg($query_string, 'paged', max(1, $current-1)),
      '&lsaquo;'
    );

    if ( 'bottom' == $which )
      $html_current_page = $current;
    else
      $html_current_page = sprintf( "<input class='current-page' title='%s' type='text' name='paged' value='%s' size='%d' />",
        'Текущая страница',
        $current,
        strlen( $total_pages )
      );

    $html_total_pages = sprintf( "<span class='total-pages'>%s</span>", $total_pages );
    $page_links[] = '<span class="paging-input">' .$html_current_page.' из '.$html_total_pages . '</span>';

    $page_links[] = sprintf( "<a class='%s' title='%s' href='%s'>%s</a>",
      'next-page' . $disable_last,
      'Перейти на следующую страницу',
      $current_uri.add_query_arg($query_string, 'paged', min( $total_pages, $current+1 )),
      '&rsaquo;'
    );

    $page_links[] = sprintf( "<a class='%s' title='%s' href='%s'>%s</a>",
      'last-page' . $disable_last,
      'Перейти на последнюю страницу',
      $current_uri.add_query_arg($query_string, 'paged', $total_pages),
      '&raquo;'
    );

    $output .= "\n<span class='pagination-links'>" . join( "\n", $page_links ) . '</span>';

    if ( $total_pages )
      $page_class = $total_pages < 2 ? ' one-page' : '';
    else
      $page_class = ' no-pages';

    $this->_pagination = "<div class='tablenav-pages{$page_class}'>$output</div>";

    echo $this->_pagination;
  }

  function get_pagenum() {
    $pagenum = isset( $_REQUEST['paged'] ) ? abs( intval( $_REQUEST['paged'] ) ) : 0;

    if( isset( $this->_pagination_args['total_pages'] ) && $pagenum > $this->_pagination_args['total_pages'] )
      $pagenum = $this->_pagination_args['total_pages'];

    return max( 1, $pagenum );
  }

  function set_pagination_args( $args ) {
    if ( ! isset($args['total_pages']) && $args['per_page'] > 0 )
      $args['total_pages'] = ceil( $args['total_items'] / $args['per_page'] );

    $this->_pagination_args = $args;
  }

  function display_rows() {
    if ( $this->has_items() ) {
      foreach ( $this->items as $item ) {
        $this->single_row($item);
      }
    }
    else {
      echo '<tr class="no-items"><td class="colspanchange" colspan="' . count($this->get_columns()) . '">';
      $this->no_items();
      echo '</td></tr>';
    }
  }

  function single_row($item) {
    static $row_class = '';
    $row_class = ( $row_class == '' ? ' class="alternate"' : '' );

    echo '<tr' . $row_class . '>';

    $columns = $this->get_columns();

    foreach ( $columns as $column_name => $column_display_name ) {
      $class = "class='$column_name column-$column_name'";

      if ( 'cb' == $column_name ) {
        echo '<th scope="row" class="check-column">';
        echo $this->column_cb( $item );
        echo '</th>';
      }
      elseif ( method_exists( $this, 'column_' . $column_name ) ) {
        echo "<td $class>";
        echo call_user_func( array( $this, 'column_' . $column_name ), $item );
        echo "</td>";
      }
    }
    echo '</tr>';
  }

  function has_items() {
    return !empty( $this->items );
  }
}
