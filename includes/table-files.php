<?php defined('ABSPATH') or exit('');

class Table_files extends Table {
	function get_columns() {
		$columns = array(
			'cb' => '<input type="checkbox" />',
			'td_name' => 'Имя файла',
      'td_size' => '<img title="Вес" src="'.URL_IMGS.'/weight.png" width="16" height="16">',
      'td_cats' => '<img title="Категории" src="'.URL_IMGS.'/cats.png" width="16" height="16">'
		);

    if(is_login()) {
//      $columns['count_down'] = '<img title="Оборванных скачиваний x Полных скачиваний" src="'.URL_IMGS.'/down.png" width="16" height="16">';
      $columns['count_down'] = '<img title="Счётчик скачиваний" src="'.URL_IMGS.'/down.png" width="16" height="16">';
      $columns['perm_down'] = '<span title="Публичный доступ">Запрет</span>';
      $columns['td_delete'] = 'Удаление';
    }
    else
      array_shift($columns);

		return $columns;
	}

	function prepare_items() {
		global $db, $core;

    $table_files = $db->table_files;
    $table_cats = $db->table_cats;
    $table_rsh = $db->cats_rships;

		$per_page = (int)$core->options['rows_per_page'];
		$current_page = (int)$this->get_pagenum();
    $s = '';

    $ext = is_field('ext') ? strtolower($_POST['ext']) : '';
    $where = is_field('ext') ? " AND extension = '$ext'" : '';

    if( ! is_login()) {
      $where .= "
      AND perm_down = 1
      AND NOT id IN (
        SELECT id
          FROM $table_cats, $table_rsh, $table_files
          WHERE id = id_file_rsh
            AND id_cat = id_cat_rsh
            AND cat_perm_down = 0
      )";
    }

    if(is_field('cat') && (int)$_REQUEST['cat'] > 0) {
      $cat = (int)$_REQUEST['cat'];
      $query_total = $this->get_sql_total(
        $table_rsh.($where ? ", $table_files" : ''),
        "id_cat_rsh = $cat".($where ? "  AND id_file_rsh = id" : '').$where
      );
      $query_items = $this->get_sql_items("$table_files, $table_rsh", "id_cat_rsh = $cat AND id_file_rsh = id".$where);
    }
    elseif(is_field('search_files')) {
      $s = '%'.sanitize_search_text($_REQUEST['search_files']).'%';
      $query_total = $this->get_sql_total($table_files, "file_name LIKE '$s'");
      $query_items = $this->get_sql_items($table_files, "file_name LIKE '|search|'");
    }
    else {
      $query_total = $this->get_sql_total($table_files, "1$where");
      $query_items = $this->get_sql_items($table_files, "1$where");
    }

		$total_items = (int)$db->get_var($query_total);
		$current_page = max(min(ceil($total_items / $per_page), $current_page), 1);

    $query_items = $db->prepare($query_items, array(($current_page-1) * $per_page, $per_page));

    if($s)
      $query_items = str_replace('|search|', $s, $query_items);

    $this->items = $db->get_results($query_items);

   	$this->set_pagination_args(array(
			'total_items' => $total_items,
			'per_page' => $per_page
		));
	}

  function single_row($item) {
    global $db;
    $table_cats = $db->table_cats;
    $table_rsh = $db->cats_rships;

    $sql = $db->_escape("SELECT id_cat FROM $table_cats, $table_rsh WHERE id_file_rsh = %d AND id_cat = id_cat_rsh AND cat_perm_down = 0");
    $cat_access = $db->get_row($db->prepare($sql, $item->id));

    if( ! is_login() && ((int)$item->perm_down == 0 || $cat_access))
      return;
    else
      parent::single_row($item);
  }

	function get_sql_total($from, $where) {
    return "SELECT COUNT(*) FROM $from WHERE $where";
	}

	function get_sql_items($from, $where) {
    return "
      SELECT id, file_name, count_down, perm_down, press_down, file_size, extension
        FROM $from
        WHERE $where
        LIMIT %d, %d";
	}

	function column_cb($item) {
		return '<input type="checkbox" class="id-item" name="file[]" value="'.$item->id.'"'.
						'data-nonce="'.create_nonce($item->id.'_edit').'" />';
	}

	function column_td_name($item) {
		$ext = strtoupper($item->extension);

		if(file_exists(PATH_ICONS.'/'.$ext.'.png'))
			$path = URL_ICONS.'/'.$ext.'.png';
		else
			$path = URL_ICONS.'/Default.png';

    $id = $item->id;
    $file_name = $item->file_name;
		$sort = '<a class="sort-ext" data-sort="'.$ext.'" href="#"><img class="thumb" src="'.$path.'" width="37" height="15"></a>';
		$rename = '<a class="rename-action" href="#">Переименовать</a><a class="save-rename-action" href="#">Сохранить</a>';
		$name = '<a class="edit-action name'.$id.'" href="'.SITEURL.'?down='.$id.'">'.$file_name.'</a><input class="field-edit-action new-name'.$id.'" type="text" value="'.$file_name.'">';

		return $sort.$name.(is_login() ? $rename : '');
	}

	function column_td_size($item) {
		return get_size($item->file_size);
	}

  function display() {
    echo nonce_field('ajax_uploaded', '_ajax_nonce');
    echo is_field('ext') ? '<input type="hidden" name="ext" value="'.$_REQUEST['ext'].'">' : '';
    parent::display();
  }

	function column_td_cats($item) {
    global $db;
    $table_cats = $db->table_cats;
    $table_rsh = $db->cats_rships;

    $id = (int)$item->id;
    $query_cats = "
      SELECT cat_name
        FROM $table_rsh, $table_cats
        WHERE id_file_rsh = $id
          AND id_cat_rsh = id_cat";
    $rs = $db->get_col($query_cats);

    $out = 'bad';
    if(is_array($rs) && ! empty($rs))
      $out = '<a href="#" title="'.implode(', ', $rs).'">'.count($rs).'</a>';
    else if(is_array($rs) && empty($rs))
      $out = '0';

		return $out;
	}

	function column_perm_down($item) {
		$access =  (bool)$item->perm_down;
		$input = '<input type="button" value="'.($access ? 'Разрешено' : 'Запрещено').'"'.
							' class="button permission '.($access ? 'yes' : 'no').'" title="Запретить или разрешить скачивание">';
		return $input;
	}

	function column_count_down($item) {
//		return ($item->press_down - $item->count_down).' x '.$item->count_down;
		return $item->count_down;
	}

	function column_td_delete($item) {
		$input = '<input type="button" data-id="'.$item->id.'" value="Удалить" class="button delete" title="Удалить файл">';
		return $input;
	}

	function get_table_classes() {
		return array('widefat', $this->_args['plural']);
	}

	function get_bulk_actions() {
    if( ! is_login())
      return array();

		$actions = array(
			'delete' => 'Удалить',
			'perm-yes' => 'Разрешить',
			'perm-no' => 'Запретить',
		);
		return $actions;
	}

  function extra_tablenav($which) {
    if('top' == $which) {
      global $db;
      $table_name = $db->table_cats;
      $cats = $db->get_results("SELECT id_cat as id, cat_name as name, cat_perm_down as perm FROM $table_name WHERE 1");

      if(is_field('cat') && (int)$_REQUEST['cat'] > 0)
        $selected = (int)$_POST['cat'];
      else
        $selected = 0;
      ?>
      <div class="alignleft actions">
      <?php if(is_login()) { ?>
        <input type="submit" name="from-cat" id="from-category" class="button action" value="Из" title="Убрать из категории ...">
        <input type="submit" name="to-cat" id="to-category" class="button action" value="В" title="Добавить в категорию ...">
      <?php } ?>
        <select name="categories" id="cat" class="postform">
          <option value="0">Все категории</option>
          <?php
            foreach($cats as $cat) {
              if( ! is_login() &&  (int)$cat->perm == 0)
                continue;

              echo '<option value="'.$cat->id.'"';
              echo ($selected == $cat->id ? ' selected="selected"' : '');
              echo '>'.$cat->name.'</option>';
            }
          ?>
        </select>
        <input type="submit" name="cat-filter" id="categories-filter" class="button action" value="Фильтр" title="Отфильтровать по категории">
        <input type="submit" name="cat-filter" id="all-categories" class="button action" value="Все категории" title="Все категории">
      </div>
      <?php
    }
  }

  function no_items() {
    echo 'Файлов не найдено.';
  }
}



