<?php defined('ABSPATH') or exit('');

/**
 * Class Ajax
 */
class Ajax {

	function send_json($data, $error = '') { // strictly compare of status
		$response = array('success' => (bool)$data);

		if($data !== false)
			$response['data'] = $data;

		if($error)
			$response['error'] = $error;

		$type = is_oldie() ? 'text/html' : 'application/json';
		@header('Content-Type: '.$type.'; charset=utf-8');
		echo json_encode($response);//, JSON_HEX_TAG

		exit();
	}

	function ajax_exit($action, $result) {
		if($result) return;
    $this->send_json(false);
	}

	function delete_file() {
		global $db, $core;
		$table_name = $db->table_files;

		if( ! is_login())
      $this->send_json(false, 'У вас нет прав доступа.');

		$id = (int)$_POST['id'];

		if( ! verify_nonce($_POST['_ajax_nonce'], $id.'_edit'))
      $this->send_json(false, 'Неверный запрос.');

		$sql = "SELECT file_name, extension FROM $table_name WHERE id = %d;";
		$file = $db->get_row($db->prepare($sql, $id));

		if(empty($file->file_name))
      $this->send_json(false, 'Этого файла нет в БД.');

		$path = $core->upload_dir.'/'.$file->file_name.'.'.$file->extension;
		if( ! file_exists($path))
      $this->send_json(false, 'Этого файла не существует.');

		@unlink($path);

		$sql = "DELETE FROM $table_name WHERE id = %d";
		$db->query($db->prepare($sql, $id));

    $this->send_json(true);
	}

	function uploaded_group_permission() {
    global $db;
    $table_name = $db->table_files;

    if( ! is_login())
      $this->send_json(false, 'У вас нет прав доступа.');

    if( ! verify_nonce($_POST['_ajax_nonce'], 'ajax_uploaded'))
      $this->send_json(false, 'Неверный запрос.');

    $access = array('perm-no', 'perm-yes');

    if( ! is_field('swtch') || ! in_array($_REQUEST['swtch'], $access))
      $this->send_json(false, 'Плохой запрос.');

    if( ! is_field('items') || ! is_array($_REQUEST['items']))
      $this->send_json(false, 'Плохой запрос.');

    $perm = $_REQUEST['swtch'] == 'perm-yes' ? 1 : 0;

    foreach($_REQUEST['items'] as $id) {
      $sql = "UPDATE $table_name SET perm_down = %d WHERE id = %d";
      $db->query($db->prepare($sql, array($perm, (int)$id)));
    }

    $this->clean_update_files_table();
    exit();
	}

	function categories_group_permission() {
    global $db;
    $table_name = $db->table_cats;

    if( ! is_login())
      $this->send_json(false, 'У вас нет прав доступа.');

    if( ! verify_nonce($_POST['_ajax_nonce'], 'ajax_categories'))
      $this->send_json(false, 'Неверный запрос.');

    $access = array('perm-no', 'perm-yes');

    if( ! is_field('swtch') || ! in_array($_REQUEST['swtch'], $access))
      $this->send_json(false, 'Плохой запрос.');

    if( ! is_field('items') || ! is_array($_REQUEST['items']))
      $this->send_json(false, 'Плохой запрос.');

    $perm = $_REQUEST['swtch'] == 'perm-yes' ? 1 : 0;

    foreach($_REQUEST['items'] as $id) {
      $sql = "UPDATE $table_name SET cat_perm_down = %d WHERE id_cat = %d";
      $db->query($db->prepare($sql, array($perm, (int)$id)));
    }

    $this->clean_update_cats_table();
    exit();
	}

	function categories_group_delete() {
    global $db;
    $table_name = $db->table_cats;

    if( ! is_login())
      $this->send_json(false, 'У вас нет прав доступа.');

    if( ! verify_nonce($_POST['_ajax_nonce'], 'ajax_categories'))
      $this->send_json(false, 'Неверный запрос.');

    if( ! is_field('items') || ! is_array($_REQUEST['items']))
      $this->send_json(false, 'Плохой запрос.');

    foreach($_REQUEST['items'] as $id) {
      $sql = "DELETE FROM $table_name WHERE id_cat = $id";
      $db->query($db->prepare($sql, (int)$id));
    }

    $this->clean_update_cats_table();
    exit();
	}

	function uploaded_group_delete() {
    global $db, $core;
    $table_name = $db->table_files;

    if( ! is_login())
      $this->send_json(false, 'У вас нет прав доступа.');

    if( ! verify_nonce($_POST['_ajax_nonce'], 'ajax_uploaded'))
      $this->send_json(false, 'Неверный запрос.');

    if( ! is_field('items') || ! is_array($_REQUEST['items']))
      $this->send_json(false, 'Плохой запрос.');


    foreach($_REQUEST['items'] as $id) {

      $id = (int)$id;
      $sql = "
        SELECT file_name, extension
          FROM $table_name WHERE id = %d";
      $file = $db->get_row($db->prepare($sql, $id));

      if( ! $file)
        continue;

      $path = $core->upload_dir.'/'.$file->file_name.'.'.$file->extension;

      if( ! file_exists($path))
        continue;

      @unlink($path);

      $sql = "DELETE FROM $table_name WHERE id = %d";
      $db->query($db->prepare($sql, $id));
    }

    $this->clean_update_files_table();
    exit();
	}

	function delete_cat() {
		global $db;
		$table_name = $db->table_cats;

		if( ! is_login())
      $this->send_json(false, 'У вас нет прав доступа.');

		$id = (int)$_POST['id'];

		if( ! verify_nonce($_POST['_ajax_nonce'], $id.'_edit'))
      $this->send_json(false, 'Неверный запрос.');

		$db->query("DELETE FROM $table_name WHERE id_cat = $id");

    $this->send_json(true);
	}

	function perm_file() {
		global $db;
		$table_name = $db->table_files;

		if( ! is_login())
      $this->send_json(false, 'У вас нет прав доступа.');

		$id = (int)$_POST['id'];

		if( ! verify_nonce($_POST['_ajax_nonce'], $id.'_edit'))
      $this->send_json(false, 'Неверный запрос.');

		$sql = "SELECT perm_down FROM $table_name WHERE id = %d";
		$perm = $db->get_var($db->prepare($sql, $id));

		$sql = "UPDATE $table_name SET perm_down = %d WHERE id = %d";

		$perm = $perm ? 0 : 1;
		if($db->query($db->prepare($sql, array($perm, $id)))) {
      $this->send_json(array(
				'perm' => $perm,
				'nonce' => create_nonce($id.'_edit')
			));
		}
		else
      $this->send_json(false);
	}

	function perm_cat() {
		global $db;
		$table_name = $db->table_cats;

		if( ! is_login())
      $this->send_json(false, 'У вас нет прав доступа.');

		$id = (int)$_POST['id'];

		if( ! verify_nonce($_POST['_ajax_nonce'], $id.'_edit'))
      $this->send_json(false, 'Неверный запрос.');

		$perm = $db->get_var("SELECT cat_perm_down FROM $table_name WHERE id_cat = $id");

		$perm = $perm ? 0 : 1;
		if($db->query("UPDATE $table_name SET cat_perm_down = $perm WHERE id_cat = $id")) {
      $this->send_json(array(
				'perm' => $perm,
				'nonce' => create_nonce($id.'_edit')
			));
		}
		else
      $this->send_json(false);
	}

	function rename_file() {
		global $db, $core;
		$table_name = $db->table_files;

		if( ! is_login())
      $this->send_json(false, 'У вас нет прав доступа.');

		$id = (int)$_POST['id'];

		if( ! verify_nonce($_POST['_ajax_nonce'], $id.'_edit'))
      $this->send_json(false, 'Неверный запрос.');

		$sql = "SELECT file_name, extension FROM $table_name WHERE id = %d";
		$rs = $db->get_row($db->prepare($sql, $id));

    if( ! $rs)
      $this->send_json(false, 'Неудача запроса в БД.');

		$current_name = $rs->file_name;

		$response = array(
			'nonce' => create_nonce($id.'_edit'),
		);

		// sanitize the file name
		$new_name = sanitize_file_name($_POST['new_name']);

		if(mb_strlen($new_name) > 240)
      $this->send_json(false, 'Имя слишком длинное.');

		if( ! isset($_POST['new_name']) || trim($_POST['new_name']) == '' || $current_name == $new_name) {
			$response['new_name'] = $current_name;
      $this->send_json($response);
		}

		// make unique name
		$new_name = unique_filename($core->upload_dir, $new_name.'.'.$rs->extension);

    $info = pathinfo($new_name);
    $ext = ! empty($info['extension']) ? $info['extension'] : '';
    $name = rtrim(basename($new_name, $ext), '.');

		$sql = "UPDATE $table_name SET file_name = %s WHERE id = %d";
		$rs2 = $db->query($db->prepare($sql, array($name, $id)));

		$old = $core->upload_dir.'/'.$current_name.'.'.$rs->extension;
		$new = $core->upload_dir.'/'.$new_name;

		if($rs2 && file_exists($old) && rename($old, $new)) {
			$response['new_name'] = $name;
      $this->send_json($response);
		}
		else
      $this->send_json(false, 'Неудача.');

		exit();
	}

	function rename_cat() {
		global $db;
		$table_name = $db->table_cats;

		if( ! is_login())
      $this->send_json(false, 'У вас нет прав доступа.');

		$id = (int)$_POST['id'];

		if( ! verify_nonce($_POST['_ajax_nonce'], $id.'_edit'))
      $this->send_json(false, 'Неверный запрос.');

    $response = array(
      'nonce' => create_nonce($id.'_edit'),
    );

		$sql = "SELECT cat_name FROM $table_name WHERE id_cat = %d";
		$current_name = $db->get_var($db->prepare($sql, $id));
    $new_name = sanitize_file_name(trim($_REQUEST['new_name']), false);

		if(mb_strlen($new_name) > 240)
      $this->send_json(false, 'Имя слишком длинное.');

    if( ! isset($_POST['new_name']) || trim($_POST['new_name']) == '' || $current_name == $new_name) {
      $response['new_name'] = $current_name;
      $this->send_json($response);
    }

    $names = $db->get_col("SELECT cat_name FROM $table_name WHERE 1");
    $new_name = unique_name($_REQUEST['new_name'], $names, false);

		$sql = "UPDATE $table_name SET cat_name = %s WHERE id_cat = %d";
		$rs = $db->query($db->prepare($sql, array($new_name, $id)));

		if($rs) {
			$response['new_name'] = $new_name;
      $this->send_json($response);
		}
		else {
      $this->send_json(false, 'Неудача.');
    }
		exit();
	}

	function to_category() {
    if( ! is_login())
      exit('У вас нет прав доступа.');

    if( ! verify_nonce($_REQUEST['_ajax_nonce'], 'ajax_uploaded'))
      exit('Неверный запрос.');

    if(is_field('submit') && $_REQUEST['submit'] == 'to-cat' && is_field('cat') && (int)$_REQUEST['cat'] > 0)
      $this->add_files_to_category();

    $this->clean_update_files_table();
    exit();
	}

	function from_category() {
    if( ! is_login())
      exit('У вас нет прав доступа.');

    if( ! verify_nonce($_REQUEST['_ajax_nonce'], 'ajax_uploaded'))
      exit('Неверный запрос.');

    if(is_field('submit') && $_REQUEST['submit'] == 'from-cat' && is_field('cat') && (int)$_REQUEST['cat'] >= 0)
      $this->delete_files_from_category();

    $this->clean_update_files_table();
    exit();
	}

	function update_table() {
//		if( ! is_login())
//      exit('У вас нет прав доступа.');

    if( ! verify_nonce($_REQUEST['_ajax_nonce'], 'ajax_uploaded'))
      exit('Неверный запрос.');

    $this->clean_update_files_table();
    exit();
	}

	function clean_update_files_table() {
    global $core;
    ob_start();
    $core->table_files = new Table_files();
    $core->tab_uploaded();
    echo ob_get_clean();
	}

	function new_category() {
    global $db;

    if( ! is_login())
      exit('У вас нет прав доступа.');

    if( ! verify_nonce($_REQUEST['_ajax_nonce'], 'ajax_categories'))
      exit('Неверный запрос.');

    if( ! is_field('submit') || $_REQUEST['submit'] != 'newcat' || ! is_field('cat'))
      exit('Ошибка запроса.');

    $table_name = $db->table_cats;
    $names = $db->get_col("SELECT cat_name FROM $table_name WHERE 1");
    $name = unique_name($_REQUEST['cat'], $names, false);

    $sql = "INSERT INTO $table_name (cat_name) VALUES ('%s')";
    $db->query($db->prepare($sql, $name));

    $this->clean_update_cats_table();

		exit();
	}

	function clean_update_cats_table() {
    global $core;
    ob_start();
    $core->table_cats = new Table_cats();
    $core->tab_categories();
    echo ob_get_clean();
	}

	function update_cats_table() {
    if( ! is_login())
      exit('У вас нет прав доступа.');

    if( ! verify_nonce($_REQUEST['_ajax_nonce'], 'ajax_categories'))
      exit('Неверный запрос.');

    $this->clean_update_cats_table();
    exit();
	}

	function add_files_to_category() {
    if(is_field('files') && is_array($_REQUEST['files'])) {
      global $db, $core;
      $table_rsh = $db->cats_rships;
      $cat = (int)$_POST['cat'];
      $files = $_POST['files'];

      $insert = '';
      foreach($files as $id_file) {
        $id_file = (int)$id_file;
        if( ! empty($insert))
          $insert .= ', ';
        $insert .= "($cat, $id_file)";
      }

      $sql = "
        INSERT IGNORE INTO $table_rsh (id_cat_rsh, id_file_rsh)
        VALUES $insert";

      $db->query($sql);
    }
	}

	function delete_files_from_category() {
    if(is_field('files') && is_array($_REQUEST['files'])) {
      global $db;
      $table_rsh = $db->cats_rships;
      $cat = (int)$_POST['cat'];
      $files = $_POST['files'];

      $and = $cat == 0 ? '' : "AND id_cat_rsh = $cat";

      foreach($files as $id_file) {
        $id_file = (int)$id_file;
        $db->query("DELETE FROM $table_rsh WHERE id_file_rsh = $id_file $and");
      }
    }
	}

	/**
	 * @param $file
	 * @return bool|mixed
	 */
	function insert_file($file) {
		global $db;
		$table_name = $db->table_files;

    extract($file);

		$sql = "
      INSERT INTO $table_name (file_name, file_size, extension, mime_type)
      VALUES ('%s', %d, '%s', '%s');";

		if($db->query($db->prepare($sql, array($name, $size, $ext, $mime_type)))) {
			return true;
		}
		return false;
	}

	/**
	 *
	 */
	function upload() {
    global $core;
		if( ! is_login())
      $this->send_json(false, 'У вас нет прав доступа.');

		if( ! verify_nonce($_POST['_ajax_nonce'], 'media_form_upload'))
      $this->send_json(false, 'Неверный запрос.');

		$file = $_FILES['ajax-upload-file'];

		if($file['error'] > 0)
      $this->send_json(false, 'Ошибка загрузки файла "'.$file['name'].'". [error] => '.$file['error']);

		if( ! ($file['size'] > 0))
      $this->send_json(false, 'Файл "'.$file['name'].'" пуст. Пожалуйста выберите другой. Кстати могут быть отключены загрузки в php.ini или post_max_size меньше чем upload_max_filesize там же в php.ini.');

		$upload_size = max_upload_size();

		if($file['size'] > $upload_size)
      $this->send_json(false, 'Размер файла "'.$file['name'].'" превышает максимальный для этого сервера.');

		if( ! @is_uploaded_file($file['tmp_name']))
      $this->send_json(false, 'Файл "'.$file['name'].'" не был загружен при помощи HTTP POST.');

    $filetype = check_filetype($file['name']);

    extract($filetype);

    if( ! $ext)
      $ext = ltrim(strrchr($file['name'], '.'), '.');

    if( ! $type)
      $type = $file['type'];

    if( ! in_array($ext, $core->allowed_ext))
      $this->send_json(false, 'Файл "'.$file['name'].'" не принадлежит к разрешённым типам.');

    $filename = unique_filename($core->upload_dir, $file['name']);

    $new_file = $core->upload_dir.'/'.$filename;

		if(false === @move_uploaded_file($file['tmp_name'], $new_file))
      $this->send_json(false, 'Файл "'.$file['name'].'" не может быть загружен в директорию: "'.$core->upload_dir.'/"');

		$stat = stat(dirname($new_file));
		$perms = $stat['mode'] & 0000666;
		@chmod($new_file, $perms);

    $info = pathinfo($filename);
    $ext = ! empty($info['extension']) ? $info['extension'] : '';
    $name = rtrim(basename($filename, $ext), '.');

		$data = array(
			'name' => $name,
			'size' => $file['size'],
			'ext' => $ext,
			'mime_type' => $type,
		);

		$total_uploads = $this->insert_file($data);

		if($total_uploads) {
      $this->send_json(array(
				'message' => 'Файл "'.$filename.'" был успешно загружен.'
			));
		}
		else
      $this->send_json(false, 'Файл "'.$filename.'" загрузить не удалось.');
	}
}
