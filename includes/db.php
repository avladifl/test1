<?php 

class DB {

  protected $last_result;
  protected $result;

  protected $charset;
  protected $collate;
  protected $dbuser;
  protected $dbpassword;
  protected $dbname;
  protected $dbhost;
  protected $dbh;

  public $table_options = 'docs_options';
  public $table_cats = 'cats';
  public $table_files = 'files';
  public $cats_rships = 'cats_relationships';

  
  // ������������� ������ ��
  function __construct( $dbuser, $dbpassword, $dbname, $dbhost ) {

    if ( defined( 'DB_COLLATE' ) )
      $this->collate = DB_COLLATE;

    if ( defined( 'DB_CHARSET' ) )
      $this->charset = DB_CHARSET;

    $this->dbuser = $dbuser;
    $this->dbpassword = $dbpassword;
    $this->dbname = $dbname;
    $this->dbhost = $dbhost;

    $this->db_connect();
  }

  // ������������� �����
  function _real_escape( $string ) {
    if ( $this->dbh )
      return mysql_real_escape_string( $string, $this->dbh );

    return addslashes( $string );
  }

  function _escape( $data ) {
    if ( is_array( $data ) ) {
      foreach ( $data as $k => $v ) {
        if ( is_array($v) )
          $data[$k] = $this->_escape( $v );
        else
          $data[$k] = $this->_real_escape( $v );
      }
    } else {
      $data = $this->_real_escape( $data );
    }

    return $data;
  }

  function escape_by_ref( &$string ) {
    if ( ! is_float( $string ) )
      $string = $this->_real_escape( $string );
  }

  function prepare( $query, $args ) {
    if ( is_null( $query ) )
      return;

    $args = func_get_args();
    array_shift( $args );

    if ( isset( $args[0] ) && is_array($args[0]) )
      $args = $args[0];
    $query = str_replace( "'%s'", '%s', $query ); // in case someone mistakenly already singlequoted it
    $query = str_replace( '"%s"', '%s', $query ); // doublequote unquoting
    $query = preg_replace( '|(?<!%)%f|' , '%F', $query ); // Force floats to be locale unaware
    $query = preg_replace( '|(?<!%)%s|', "'%s'", $query ); // quote the strings, avoiding escaped strings like %%s
    array_walk( $args, array( $this, 'escape_by_ref' ) );

    return vsprintf( $query, $args );
  }

  function flush() {
    $this->last_result = array();
    $this->rows_affected = 0;
  }

  function db_connect() {
    $this->dbh = mysql_connect( $this->dbhost, $this->dbuser, $this->dbpassword, true, 0 );
//    $this->dbh = @mysql_connect( $this->dbhost, $this->dbuser, $this->dbpassword, true, 0 );

    if ( !$this->dbh ) {
      exit('error connect to db');
    }

    // set charset
    if ( ! empty( $this->charset ) ) {
      if ( function_exists( 'mysql_set_charset' ) ) {
        mysql_set_charset( $this->charset, $this->dbh );
      }
      else {
        $query = $this->prepare( 'SET NAMES %s', $this->charset );
        if ( ! empty( $this->collate ) )
          $query .= $this->prepare( ' COLLATE %s', $this->collate );
        mysql_query( $query, $this->dbh );
      }
    }

    $this->ready = true;

    if ( !@mysql_select_db( $this->dbname, $this->dbh ) ) {
      $this->ready = false;
      exit('error of select db');
    }
  }

  function query( $query ) {
    if ( ! $this->ready )
      return false;

    $return_val = 0;
    $this->flush();

    $this->result = @mysql_query( $query, $this->dbh );

    if ( $last_error = mysql_error( $this->dbh ) ) {
      $err = 'mysql error: "'.$last_error.'"'.' $query: '.$query.'<br>';

      $trace = array();
      foreach(debug_backtrace() as $func) {
        $trace[] = $func['function'].' '.$func['line'].' '.basename($func['file']);
      }

//      $out = $err.'<pre>'.print_r(debug_backtrace(), true).'<pre>';
      $out = $err.'<pre>'.implode('<br>', $trace).'<pre>';

      header('Content-Type: text/html; charset=utf-8');
      exit($out);
    }

    if ( preg_match( '/^\s*(create|drop)\s/i', $query ) ) {
      $return_val = $this->result;
    }
    elseif ( preg_match( '/^\s*(insert|delete|update|replace)\s/i', $query ) ) {
      $return_val = mysql_affected_rows( $this->dbh );
    }
    else {
      $num_rows = 0;
      while ( $row = @mysql_fetch_object( $this->result ) ) {
        $this->last_result[$num_rows] = $row;
        $num_rows++;
      }
    }

    return $return_val;
  }

  function get_var( $query = null, $x = 0, $y = 0 ) {
    if ( $query )
      $this->query( $query );

    if ( !empty( $this->last_result[$y] ) ) {
      $values = array_values( get_object_vars( $this->last_result[$y] ) );
    }

    return ( isset( $values[$x] ) && $values[$x] !== '' ) ? $values[$x] : null;
  }

  function get_row( $query = null, $n = 0 ) {

    if ( $query )
      $this->query( $query );
    else
      return null;

    if ( !isset( $this->last_result[$n] ) )
      return null;

    return $this->last_result[$n] ? $this->last_result[$n] : null;
  }

  function get_col( $query = null , $x = 0 ) {
    if ( $query )
      $this->query( $query );

    $new_array = array();

    for ( $i = 0, $j = count( $this->last_result ); $i < $j; $i++ ) {
      $new_array[$i] = $this->get_var( null, $x, $i );
    }

    return $new_array;
  }

  function get_results($query = null) {
    if($query)
      $this->query( $query );
    else
      return null;

    return $this->last_result;
  }
}
