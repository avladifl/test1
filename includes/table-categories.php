<?php defined('ABSPATH') or exit('');

class Table_cats extends Table {
	function get_columns() {
		$columns = array(
			'cb' => '<input type="checkbox" />',
			'td_name' => 'Имя категории',
			'cat_files' => '<span title="Количество файлов">Файлы</span>',
			'cat_sizes' => '<span title="Общий вес">Вес</span>',
			'cat_perm_down' => 'Запрет',
			'cat_delete' => 'Удаление',
		);
		return $columns;
	}

	function prepare_items() {
		global $db, $core;

		$table_cats = $db->table_cats;

		$per_page = (int)$core->options['rows_per_page'];
		$current_page = (int)$this->get_pagenum();

		$query_total = 'SELECT COUNT(*) FROM '.$table_cats.' WHERE 1;';
    $query_items = 'SELECT id_cat, cat_name, cat_perm_down FROM '.$table_cats.' WHERE 1 LIMIT %d, %d;';

    $s = '';
    if(is_field('search_cats')) {
      $s = '%'.sanitize_search_text($_REQUEST['search_cats']).'%';
      $query_total = "SELECT COUNT(*) FROM $table_cats WHERE cat_name LIKE '$s'";
      $query_items = "SELECT id_cat, cat_name, cat_perm_down FROM $table_cats WHERE cat_name LIKE '|search|' LIMIT %d, %d";
    }

		$total_items = (int)$db->get_var($query_total);
		$current_page = max(min(ceil($total_items / $per_page), $current_page), 1);

    $query_items = $db->prepare($query_items, array(($current_page-1) * $per_page, $per_page));

    if($s)
      $query_items = str_replace('|search|', $s, $query_items);

		$this->items = $db->get_results($query_items);

		$this->set_pagination_args(array(
			'total_items' => $total_items,
			'per_page' => $per_page
		));
  }

	function column_cb($item) {
    global $core;
		return '<input type="checkbox" class="id-item" name="cats[]" value="'.$item->id_cat.'"'.
						'data-nonce="'.create_nonce($item->id_cat.'_edit').'" />';
	}

	function display() {
    echo nonce_field('ajax_categories', '_ajax_nonce');
		parent::display();
	}

	function column_td_name($item) {
		$rename = '<a class="rename-action" href="#">Переименовать</a><a class="save-rename-action" href="#">Сохранить</a>';
		$name = '<a class="edit-action name'.$item->id_cat.'" href="#">'.$item->cat_name.'</a><input class="field-edit-action new-name'.$item->id_cat.'" type="text" value="'.$item->cat_name.'">';
    return $name.$rename;
	}

	function column_cat_files($item) {
    global $db;

    $table_files = $db->table_files;
    $table_rsh = $db->cats_rships;
    $id = (int)$item->id_cat;

    $query = "
      SELECT SUM(file_size)
        FROM $table_files, $table_rsh
        WHERE id_cat_rsh = %d
          AND id_file_rsh = id
      UNION
      SELECT COUNT(*)
        FROM $table_files, $table_rsh
        WHERE id_cat_rsh = %d
          AND id_file_rsh = id";

    $fs = $db->get_col($db->prepare($query, array($id, $id)));

    if($fs) {
      $item->fs = $fs;
      $files = $fs[1];
    }
    else
      $files = 0;

    return $files;
	}

	function column_cat_sizes($item) {
    if(isset($item->fs) && $item->fs)
      $sizes = get_size((int)$item->fs[0]);
    else
      $sizes = 0;

		return $sizes;
	}

	function column_cat_perm_down($item) {
		$access =  (bool)$item->cat_perm_down;
		$input = '<input type="button" value="'.($access ? 'Разрешено' : 'Запрещено').'"'.
							' class="button permission '.($access ? 'yes' : 'no').'" title="Запретить или разрешить скачивание из категории">';
		return $input;
	}

	function column_cat_delete($item) {
		$input = '<input type="button" data-id="'.$item->id_cat.'" value="Удалить" class="button delete" title="Удалить файл">';
		return $input;
	}

	function get_table_classes() {
		return array('widefat', $this->_args['plural']);
	}

	function get_bulk_actions() {
		$actions = array(
			'delete' => 'Удалить',
			'perm-yes' => 'Разрешить',
			'perm-no' => 'Запретить',
		);
		return $actions;
	}

  function no_items() {
    echo 'Категорий не найдено.';
  }
}



