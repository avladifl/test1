<?php defined('ABSPATH') or exit('');

function is_login() {
  global $core;
  return $core->login;
}

$pluralElem = ['элемент', 'элемента', 'элементов'];
function plural($n) {
  return (($n==1) || ($n%10==1 && $n%100!=11)) ? 0 : (($n%10>=2 && $n%10<=4 && ($n%100<10 || $n%100>=20)) ? 1 : 2);
}

function av_die($msg) { ?>
  <html lang="ru">
    <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>Docs.cs</title>
      <link rel="stylesheet" type="text/css" href="http://docs.cs/public/styles.css">
    </head>
    <body>
      <div class="wrap">
        <h3 style="float:none;"><?php echo $msg; ?></h3>
      </div>
    </body>
  </html> <?php
  exit();
}

function get_size($bytes) {
  $sizes = array('KB', 'MB', 'GB');
  for($u = -1; $bytes > 1024 && $u < count($sizes) - 1; $u++)
    $bytes /= 1024;

  if($u < 0) {
    $bytes = 0;
    $u = 0;
  }
  else {
    $bytes = (int)$bytes;
  }
  $size = $bytes.' '.$sizes[$u];

  return $size;
}

function unique_name($name, $names, $white = true) {
  $name = sanitize_file_name(trim($name), $white);
  $name = empty($name) ? '_' : $name;

  $number = 0;
  $temp = '';

  while(array_search($name.$temp, $names) !== false) {
    $temp = ++$number;
  }
  return $name.$temp;
}

function is_oldie() {
  global $is_IE;
  if($is_IE && preg_match('#MSIE (5|6|7|8|9)#', $_SERVER['HTTP_USER_AGENT'], $arr))
    return true;

  return false;
}

function is_field($field) {
  if(isset($_REQUEST[$field])) {
    if( ! empty($_REQUEST[$field]) || $_REQUEST[$field] === '0')
      return true;
  }

  return false;
}

function get_option($option) {
  global $db;

  if(is_empty($option))
    return false;

  $sql = "
    SELECT option_value
      FROM $db->table_options
      WHERE option_name = %s LIMIT 1";

  $row = $db->get_row( $db->prepare($sql, $option ) );

  if( ! $row)
    return null;

  $value = $row->option_value;

  // is serialize
  $lastc = $value[strlen($value) - 1];
  if ( ';' === $lastc || '}' === $lastc )
    $value = @unserialize( $value );

  return $value;
}

function add_option($option, $value = '') {
  global $db;

  if(is_empty($option))
    return false;

  if ( is_array( $value ) || is_object( $value ) )
    $value = serialize( $value );

  if(get_option($option))
    delete_option($option);

  $sql = "
    INSERT INTO $db->table_options (option_name, option_value)
    VALUES (%s, %s)
      ON DUPLICATE KEY UPDATE
      option_name = VALUES(option_name),
      option_value = VALUES(option_value)";

  if( ! $db->query($db->prepare($sql, $option, $value)))
    return false;

  return true;
}

function delete_option($option) {
  global $db;

  if(is_empty($option))
    return false;

  $sql = "DELETE FROM $db->table_options WHERE option_name = %s";
  if( ! $db->query($db->prepare($sql, $option)))
    return false;

  return true;
}

function is_empty($ob) {
  $ob = trim($ob);
  if(empty($ob))
    return true;
  return false;
}

function is_mobile() {
  static $is_mobile;

  if ( isset($is_mobile) )
    return $is_mobile;

  if ( empty($_SERVER['HTTP_USER_AGENT']) ) {
    $is_mobile = false;
  } elseif ( strpos($_SERVER['HTTP_USER_AGENT'], 'Mobile') !== false // many mobile devices (all iPhone, iPad, etc.)
    || strpos($_SERVER['HTTP_USER_AGENT'], 'Android') !== false
    || strpos($_SERVER['HTTP_USER_AGENT'], 'Silk/') !== false
    || strpos($_SERVER['HTTP_USER_AGENT'], 'Kindle') !== false
    || strpos($_SERVER['HTTP_USER_AGENT'], 'BlackBerry') !== false
    || strpos($_SERVER['HTTP_USER_AGENT'], 'Opera Mini') !== false
    || strpos($_SERVER['HTTP_USER_AGENT'], 'Opera Mobi') !== false ) {
    $is_mobile = true;
  } else {
    $is_mobile = false;
  }

  return $is_mobile;
}

function device_can_upload() {
  if ( ! is_mobile() )
    return true;

  $ua = $_SERVER['HTTP_USER_AGENT'];

  if ( strpos($ua, 'iPhone') !== false
    || strpos($ua, 'iPad') !== false
    || strpos($ua, 'iPod') !== false ) {
    return preg_match( '#OS ([\d_]+) like Mac OS X#', $ua, $version ) && version_compare( $version[1], '6', '>=' );
  }

  return true;
}

function max_upload_size() {
  $u_bytes = convert_hr_to_bytes( ini_get( 'upload_max_filesize' ) );
  $p_bytes = convert_hr_to_bytes( ini_get( 'post_max_size' ) );
  $bytes   = min( $u_bytes, $p_bytes );
  return $bytes;
}

function convert_hr_to_bytes( $size ) {
  $size  = strtolower( $size );
  $bytes = (int) $size;
  if ( strpos( $size, 'k' ) !== false )
    $bytes = intval( $size ) * 1024;
  elseif ( strpos( $size, 'm' ) !== false )
    $bytes = intval($size) * 1024 * 1024;
  elseif ( strpos( $size, 'g' ) !== false )
    $bytes = intval( $size ) * 1024 * 1024 * 1024;
  return $bytes;
}

function check_filetype($filename) {
  $mimes = get_mime_types();
  $type = false;
  $ext = false;

  foreach ( $mimes as $ext_preg => $mime_match ) {
    $ext_preg = '!\.(' . $ext_preg . ')$!i';
    if ( preg_match( $ext_preg, $filename, $ext_matches ) ) {
      $type = $mime_match;
      $ext = $ext_matches[1];
      break;
    }
  }

  return compact( 'ext', 'type' );
}

function get_mime_types() {
  // Accepted MIME types are set here as PCRE unless provided.
  return array(
    // Image formats
    'jpg|jpeg|jpe' => 'image/jpeg',
    'gif' => 'image/gif',
    'png' => 'image/png',
    'bmp' => 'image/bmp',
    'tif|tiff' => 'image/tiff',
    'ico' => 'image/x-icon',
    // Video formats
    'asf|asx' => 'video/x-ms-asf',
    'wmv' => 'video/x-ms-wmv',
    'wmx' => 'video/x-ms-wmx',
    'wm' => 'video/x-ms-wm',
    'avi' => 'video/avi',
    'divx' => 'video/divx',
    'flv' => 'video/x-flv',
    'mov|qt' => 'video/quicktime',
    'mpeg|mpg|mpe' => 'video/mpeg',
    'mp4|m4v' => 'video/mp4',
    'ogv' => 'video/ogg',
    'webm' => 'video/webm',
    'mkv' => 'video/x-matroska',
    // Text formats
    'txt|asc|c|cc|h' => 'text/plain',
    'csv' => 'text/csv',
    'tsv' => 'text/tab-separated-values',
    'ics' => 'text/calendar',
    'rtx' => 'text/richtext',
    'css' => 'text/css',
    'htm|html' => 'text/html',
    // Audio formats
    'mp3|m4a|m4b' => 'audio/mpeg',
    'ra|ram' => 'audio/x-realaudio',
    'wav' => 'audio/wav',
    'ogg|oga' => 'audio/ogg',
    'mid|midi' => 'audio/midi',
    'wma' => 'audio/x-ms-wma',
    'wax' => 'audio/x-ms-wax',
    'mka' => 'audio/x-matroska',
    // Misc application formats
    'rtf' => 'application/rtf',
    'js' => 'application/javascript',
    'pdf' => 'application/pdf',
    'swf' => 'application/x-shockwave-flash',
    'class' => 'application/java',
    'tar' => 'application/x-tar',
    'zip' => 'application/zip',
    'gz|gzip' => 'application/x-gzip',
    'rar' => 'application/rar',
    '7z' => 'application/x-7z-compressed',
    'exe' => 'application/x-msdownload',
    // MS Office formats
    'doc' => 'application/msword',
    'pot|pps|ppt' => 'application/vnd.ms-powerpoint',
    'wri' => 'application/vnd.ms-write',
    'xla|xls|xlt|xlw' => 'application/vnd.ms-excel',
    'mdb' => 'application/vnd.ms-access',
    'mpp' => 'application/vnd.ms-project',
    'docx' => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
    'docm' => 'application/vnd.ms-word.document.macroEnabled.12',
    'dotx' => 'application/vnd.openxmlformats-officedocument.wordprocessingml.template',
    'dotm' => 'application/vnd.ms-word.template.macroEnabled.12',
    'xlsx' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
    'xlsm' => 'application/vnd.ms-excel.sheet.macroEnabled.12',
    'xlsb' => 'application/vnd.ms-excel.sheet.binary.macroEnabled.12',
    'xltx' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.template',
    'xltm' => 'application/vnd.ms-excel.template.macroEnabled.12',
    'xlam' => 'application/vnd.ms-excel.addin.macroEnabled.12',
    'pptx' => 'application/vnd.openxmlformats-officedocument.presentationml.presentation',
    'pptm' => 'application/vnd.ms-powerpoint.presentation.macroEnabled.12',
    'ppsx' => 'application/vnd.openxmlformats-officedocument.presentationml.slideshow',
    'ppsm' => 'application/vnd.ms-powerpoint.slideshow.macroEnabled.12',
    'potx' => 'application/vnd.openxmlformats-officedocument.presentationml.template',
    'potm' => 'application/vnd.ms-powerpoint.template.macroEnabled.12',
    'ppam' => 'application/vnd.ms-powerpoint.addin.macroEnabled.12',
    'sldx' => 'application/vnd.openxmlformats-officedocument.presentationml.slide',
    'sldm' => 'application/vnd.ms-powerpoint.slide.macroEnabled.12',
    'onetoc|onetoc2|onetmp|onepkg' => 'application/onenote',
    // OpenOffice formats
    'odt' => 'application/vnd.oasis.opendocument.text',
    'odp' => 'application/vnd.oasis.opendocument.presentation',
    'ods' => 'application/vnd.oasis.opendocument.spreadsheet',
    'odg' => 'application/vnd.oasis.opendocument.graphics',
    'odc' => 'application/vnd.oasis.opendocument.chart',
    'odb' => 'application/vnd.oasis.opendocument.database',
    'odf' => 'application/vnd.oasis.opendocument.formula',
    // WordPerfect formats
    'wp|wpd' => 'application/wordperfect',
    // iWork formats
    'key' => 'application/vnd.apple.keynote',
    'numbers' => 'application/vnd.apple.numbers',
    'pages' => 'application/vnd.apple.pages',
  );
}

function unique_filename($dir, $filename) {
  $filename = sanitize_file_name(trim($filename));

  $info = pathinfo($filename);
  $ext = !empty($info['extension']) ? '.' . $info['extension'] : '';
  $name = basename($filename, $ext);

  if ( $name === $ext )
    $name = '';

  $number = '';
  $temp = '';

  $ext = strtolower($ext);

  while ( file_exists( $dir . "/$name$temp$ext" ) ) {
    $temp = ++$number;
  }

  return "$name$temp$ext";
}

function sanitize_file_name( $filename, $white = true) {
  $special_chars = array("?", "[", "]", "/", "\\", "=", "<", ">", ":", ";", ",", "'", "\"", "&", "$", "#", "*", "(", ")", "|", "~", "`", "!", "{", "}", chr(0));

  $filename = str_replace($special_chars, '', $filename);

  if($white)
    $filename = preg_replace('/[\s-]+/', '-', $filename);

  $filename = trim($filename, '.-_');

  return $filename;
}

function sanitize_search_text( $text ) {
  return sanitize_file_name($text);
}

///////////////////// NONCE /////////////////////////// 

function create_nonce($action = -1) {
  $i = nonce_tick();
  return substr(avhash($i.'|'.$action), -12, 10);
}

function avhash($data) {
  $salt = 'salt';
  return hash_hmac('md5', $data, $salt);
}

function verify_nonce($nonce, $action = -1) {
  $i = nonce_tick();

  // Nonce generated 0-12 hours ago
  $expected = substr( avhash( $i . '|' . $action), -12, 10 );

  $ex_length = strlen( $expected );
  if ( $ex_length !== strlen( $nonce ) )
    return false;

  $result = 0;

  // Do not attempt to "optimize" this.
  for ( $i = 0; $i < $ex_length; $i++ ) {
    $result |= ord( $expected[ $i ] ) ^ ord( $nonce[ $i ] );
  }

  return $result === 0;
}

function nonce_tick() {
  $day_in_seconds = 60 * 60 * 24; // nonce life
  return ceil(time() / ( $day_in_seconds / 2 ));
}

function nonce_field($action = -1, $name = "_nonce") {
  $nonce_field = '<input type="hidden" id="' . $name . '" name="' . $name . '" value="' . create_nonce( $action ) . '" />';
  return $nonce_field;
}


////////////////////////////


function add_query_arg($query = null, $k, $v = '') {
//  if($query == null)
//    $query = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];

  parse_str($query, $args);

  $t = array();
  foreach($args as $ak => $av) {
    if($v === false && $ak == $k)
      continue;
    $t[$ak] = $ak == $k ? $v : $av;
  }

  if($v != false && ! isset($t[$k]))
    $t[$k] = $v;

  return http_build_query($t);
}

/**
 * Navigates through an array and encodes the values to be used in a URL.
 *
 *
 * @since 2.2.0
 *
 * @param array|string $value The array or string to be encoded.
 * @return array|string $value The encoded array (or string from the callback).
 */
function urlencode_deep($value) {
  $value = is_array($value) ? array_map('urlencode_deep', $value) : urlencode($value);
  return $value;
}