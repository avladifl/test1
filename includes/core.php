<?php defined('ABSPATH') or exit('');


// Scope
class Core {
  public $upload_dir = '';
  public $options = array();
  public $allowed_ext = array();
  public $login = false;
  public $login_key = false;

	function __construct() {}

  function init() {
    $upload_dir = str_replace( '//', '/', UPLOADS_DIR );
    $upload_dir = rtrim($upload_dir, '/');
    $this->upload_dir = $upload_dir;
    $this->login_key = md5('login');

    // manual management the installation
    if(isset($_GET['install']))
      $this->install();

    if(isset($_GET['uninstall']))
      $this->uninstall();

    if(isset($_GET['install']) || isset($_GET['uninstall'])) {
      header('Location: '.SITEURL);
      exit();
    }

    if( ! $this->is_install()) {
      exit('necessary install db <a href="?install">install</a>');
    }
    // end manual management the installation

    $this->options = get_option('settings_downloads');

    if($this->options && isset($this->options['extensions']) && ! empty($this->options['extensions']))
      $this->allowed_ext = explode(' ', $this->options['extensions']);

    $this->request();

  }

	function request() {
    global $ajax;
		
    if(isset($_COOKIE[$this->login_key]) && $_COOKIE[$this->login_key] == md5('1'))
      $this->login = true;

    // ajax request
		if(is_field('ajax')) {
			if( ! is_field('action'))
				return;

			// Register ajax calls.
			$actions = array(
        'upload', 'update_table', 'update_cats_table',
        'rename_file', 'rename_cat',
        'uploaded_group_permission', 'categories_group_permission',
        'uploaded_group_delete', 'categories_group_delete',
        'perm_file', 'delete_file', 'perm_cat', 'delete_cat',
        'new_category', 'categories_filter', 'to_category', 'from_category'
      );

			$action = $_REQUEST['action'];

//      sleep(1);

			if(in_array($action, $actions)) {
        call_user_func(array($ajax, $action));
      }
      exit();
		}

    // registration
		elseif(is_field('password') || isset($_REQUEST['logout'])) {
      if(is_field('password') && $_SERVER['REQUEST_METHOD'] == 'POST') {
        if(isset($_POST['password']) && $_POST['password'] == '113')
          setcookie($this->login_key, md5('1'));
        else
          add_option('error_login', 'Неверный пароль');
      }
      elseif(isset($_REQUEST['logout'])) {
        setcookie($this->login_key, '', time() - 3600);
      }
      header('Location: '.SITEURL);
    }

    // download request
		elseif(is_field('down') && (int)$_GET['down'] > 0) {
      $this->file((int)$_GET['down']);
    }

    // settings request
		elseif(is_field('submit_settings') && is_field('settings_downloads') && is_array($_REQUEST['settings_downloads'])) {
      $this->add_settings($_REQUEST['settings_downloads']);

      if(is_field('_http_referer'))
        $ref = $_REQUEST['_http_referer'];
      else
        $ref = SITEURL;

      header('Location: '.$ref);
      exit();
    }

    // default request
		else {
      $this->load_page();
    }
	}

  function file($id) {
    global $db;
    $table_files = $db->table_files;
    $table_cats = $db->table_cats;
    $table_rsh = $db->cats_rships;

    if( ! is_numeric($id))
      return;

    $id = (int)$id;
    $sql = $db->_escape("SELECT file_name, perm_down, extension, mime_type FROM $table_files WHERE id = %d;");
    $filedata = $db->get_row($db->prepare($sql, $id));

    $sql = $db->_escape("SELECT id_cat FROM $table_cats, $table_rsh WHERE id_file_rsh = %d AND id_cat = id_cat_rsh AND cat_perm_down = 0");
    $cat_access = $db->get_row($db->prepare($sql, $id));

    if( ! isset($filedata->file_name) || ! $filedata->file_name)
      av_die('Такого файла нет.');

    if($cat_access || (int)$filedata->perm_down == 0)
      av_die('Файл не доступен для скачивания.');

    $fullname = $this->upload_dir.'/'.$filedata->file_name.'.'.$filedata->extension;

    if( ! file_exists($fullname))
      av_die('Файла не существует.');

    $sql = "UPDATE $table_files SET press_down = press_down + 1 WHERE id = %d";
    $db->query($db->prepare($sql, $id));

    $filesize = filesize($fullname);

    header('Content-Description: File Transfer');
    header('Content-Type: '.$filedata->mime_type);
    header('Content-Disposition: attachment; filename="'.$filedata->file_name.'.'.$filedata->extension.'"');
    header('Content-Transfer-Encoding: binary');
    header('Content-Length: '.$filesize);

    $temp = 0;
    $chunk = 1024 * 8;
    $file = @fopen($fullname, 'rb');
    $remain = $filesize;

    if($file) {
      while( !feof($file) && !connection_status()) {
        if($remain < $chunk && $remain > 0)
          $chunk = $remain;

        echo fread($file, $chunk);
        @flush();
        @ob_flush();

        $temp += $chunk;
        $remain = $filesize - $temp;
      }
      @fclose($file);

      $sql = "UPDATE $table_files SET count_down = count_down + 1 WHERE id = %d";
      $db->query($db->prepare($sql, $id));
    }
    exit();
  }

  function is_install() {
    global $db;
    $table_options = $db->table_options;
    if($db->get_var("show tables like '$table_options'") == $table_options)
      return true;
    else
      return false;
  }

  function install() {
		global $db;
		$table_files_name = $db->table_files;
		if($db->get_var("show tables like '$table_files_name'") != $table_files_name) {
			$sql = "
      CREATE TABLE $table_files_name (
        id INT UNSIGNED NOT NULL AUTO_INCREMENT,
        file_name VARCHAR(250) NOT NULL DEFAULT '',
        count_down INT UNSIGNED NOT NULL DEFAULT 0,
        press_down INT UNSIGNED NOT NULL DEFAULT 0,
        perm_down INT NOT NULL DEFAULT 1,
        file_size BIGINT UNSIGNED NOT NULL DEFAULT 0,
        extension VARCHAR(20) NOT NULL default '',
        mime_type VARCHAR(100) NOT NULL default '',
        PRIMARY KEY (id)
      ) ENGINE = INNODB CHARSET = utf8";
			$db->query($sql);
		}

		$table_cats_name = $db->table_cats;
		if($db->get_var("show tables like '$table_cats_name'") != $table_cats_name) {
			$sql = "
      CREATE TABLE $table_cats_name (
        id_cat INT UNSIGNED NOT NULL AUTO_INCREMENT,
        cat_name VARCHAR(250) NOT NULL DEFAULT '',
        cat_perm_down INT NOT NULL DEFAULT 1,
        PRIMARY KEY (id_cat)
      ) ENGINE = INNODB CHARSET = utf8";
			$db->query($sql);
		}

		$cats_relationships = $db->cats_rships;
		if($db->get_var("show tables like '$cats_relationships'") != $cats_relationships) {
			$sql = "
      CREATE TABLE $cats_relationships (
        id_cat_rsh INT UNSIGNED NOT NULL DEFAULT 0,
        id_file_rsh INT UNSIGNED NOT NULL DEFAULT 0,
        UNIQUE KEY (id_cat_rsh, id_file_rsh),
        FOREIGN KEY (id_file_rsh) REFERENCES files (id) ON DELETE CASCADE ON UPDATE CASCADE,
        FOREIGN KEY (id_cat_rsh) REFERENCES cats (id_cat) ON DELETE CASCADE ON UPDATE CASCADE
      ) ENGINE = INNODB CHARSET = utf8";
			$db->query($sql);
		}

		$table_options = $db->table_options;
		if($db->get_var("show tables like '$table_options'") != $table_options) {
			$sql = "
      CREATE TABLE IF NOT EXISTS $table_options (
        id_option INT UNSIGNED NOT NULL AUTO_INCREMENT,
        option_name VARCHAR(64) NOT NULL DEFAULT '',
        option_value LONGTEXT NOT NULL,
        PRIMARY KEY (id_option)
      ) ENGINE = INNODB CHARSET = utf8";
			$db->query($sql);
		}

		if( ! file_exists($this->upload_dir)) {
      if ( ! @mkdir( $this->upload_dir, 0777, true ) )
        exit('directory "'.$this->upload_dir.'" not created');

      file_put_contents($this->upload_dir.'/.htaccess', "RewriteEngine On\n<Files *>\nDeny from all\n</Files>");
    }

		if(empty($this->options)) {
			add_option('settings_downloads', array(
				'extensions' => 'jpg jpeg gif png rar zip doc docx pdf djvu ppt pptx txt',
				'rows_per_page' => 20
			));
		}
	}

	function uninstall() {
		global $db;

    $db->query("DROP TABLE IF EXISTS ".$db->cats_rships);
    $db->query("DROP TABLE IF EXISTS ".$db->table_cats);
		$db->query("DROP TABLE IF EXISTS ".$db->table_files);
		$db->query("DROP TABLE IF EXISTS ".$db->table_options);

		$dir = $this->upload_dir;

		if(file_exists($dir)) {
			$files = array_diff(scandir($dir), array('..', '.'));
			foreach($files as $file) {
				if(is_file($dir.'/'.$file))
					@unlink($dir.'/'.$file);
			}
		}

		@rmdir($dir);
	}

	function load_page() {
		$this->table_files = new Table_files();
		$this->table_cats = new Table_cats();

    include INCLUDES.'/header.php';
    ?>
      <div class="wrap">
        <header>
          <h3>Сервис хранения документов</h3>
          <?php if(is_login()) { ?>
            <input type="submit" value="Выйти"
                   onclick="window.location.href = '<?php echo SITEURL.'?logout'; ?>'"
                   class="logout button"/>
          <?php } else { ?>
            <form action="<?php echo SITEURL; ?>" method="post" class="login">
              <?php if($err = get_option('error_login')) { ?>
                <span class="err-login"><?php echo $err; ?></span>&nbsp;
                <?php delete_option('error_login'); ?>
              <?php } ?>
              <input type="password" name="password" placeholder="password"
                     id="password" value="" class="widefat"/>
              <input type="submit" value="Войти" class="button"
                     onclick="
                     if(document.getElementById('password')
                     .value.search(/^(\s+)?$/) == 0) return false;"/>
              <input type="hidden" name="_nonce" value="<?php echo create_nonce('login'); ?>"/>
            </form>
          <?php } ?>
        </header>
          <h2 class="nav-tab-wrapper" id="av-tabs">
            <?php if(is_login()) { ?>
              <a class="nav-tab nav-tab-active" id="upload-tab" href="#">Загрузить</a>
            <?php } ?>
              <a class="nav-tab" id="uploaded-tab" href="#">Файлы</a>
            <?php if(is_login()) { ?>
              <a class="nav-tab" id="categories-tab" href="#">Категории</a>
              <a class="nav-tab" id="settings-tab" href="#">Настройки</a>
              <a class="nav-tab" id="description-tab" href="#">Описание</a>
            <?php } ?>
          </h2>

          <?php if(is_login()) { ?>
            <div class="av-tab-screen upload" data-target="upload">
              <?php $this->tab_upload(); ?>
            </div>
          <?php } ?>
            <div class="av-tab-screen uploaded<?php echo (is_login() ? '' : ' public'); ?>"
                 data-target="uploaded">
              <?php $this->tab_uploaded(); ?>
            </div>
          <?php if(is_login()) { ?>
            <div class="av-tab-screen categories" data-target="categories">
              <?php $this->tab_categories(); ?>
            </div>
            <div class="av-tab-screen settings" data-target="settings">
              <?php $this->tab_settings(); ?>
            </div>
            <div class="av-tab-screen description" data-target="description">
              <?php $this->tab_description(); ?>
            </div>
          <?php } ?>
          <div class="shadow"><span>
              <img id="avSpinner" src="<?php echo URL_IMGS; ?>/spinner-2x.gif">
          </span></div>
          <div style="display:none;">
            <img class="spinner-small" src="<?php echo URL_IMGS; ?>/spinner.gif">
            <img class="loader-horizontal" src="<?php echo URL_IMGS; ?>/loader.gif">
          </div>
      </div>
      <script>
        var ajaxurl = '<?php echo SITEURL; ?>?ajax=1';
        avdownPage(jQuery);
      </script>
        <?php
    include INCLUDES.'/footer.php';
	}

	function add_settings($settings) {

    if( ! is_login())
      av_die('У вас нет прав доступа.');

    if( ! verify_nonce($_POST['_nonce'], 'settings'))
      av_die('Неверный запрос.');

		$sets = (array)get_option('settings_downloads');

		if( ! preg_match('#^\s*([a-z]{1,10}\s+)+[a-z]{1,10}\s*$#', $settings['extensions']))
			add_option('error_settings_ext', 'Расширения введены не верно.');
		else
			$sets['extensions'] = trim($settings['extensions']);

		$sets['rows_per_page'] = max(abs((int)$settings['rows_per_page']), 1);

		add_option('settings_downloads', $sets);
	}

	function tab_upload() {
		$this->media_upload_form();
	}

	function tab_uploaded() {
		$this->table_files->prepare_items();

    echo '<div class="layout1">';
		$this->table_files->search_box('Найти', 'search_files');
    echo '</div>';
		$this->table_files->display();
	}

	function tab_categories() { ?>
    <div class="layout1">
      <div class="create-category">
        <input class="widefat" type="text" name="newcat" value="" id="newcatInput" placeholder="Категория">
        <input type="submit" name="" class="button action" value="Создать" id="newcat">
      </div>
      <?php $this->table_cats->search_box('Найти', 'search_cats'); ?>
    </div>
    <?php
		$this->table_cats->prepare_items();
		$this->table_cats->display();
	}

	function tab_settings() { ?>
		<form action="<?php echo SITEURL; ?>" method="post">
			<?php
			  $sets = $this->options;
        $error_ext = get_option('error_settings_ext');
        delete_option('error_settings_ext');
        echo nonce_field('settings', '_nonce', true);
        echo '<input type="hidden" name="_http_referer" value="'.SITEURL.'#settings" />';
      ?>
			<p><label class="label-header" for="extensions">Введите допустимые расширения через пробел. Например <code>jpg jpeg gif png rar zip</code></label></p>
			<p><input class="widefat" type="text"
                name="settings_downloads[extensions]"
                value="<?php echo (isset($sets['extensions']) ? $sets['extensions'] : ''); ?>">
				<?php echo ( ! empty($error_ext) ? '<p class="settings-error">'.$error_ext.'</p>' : ''); ?>
			<hr/></p>

      <p><input style="width:50px;" id="rows-per-page"
               name="settings_downloads[rows_per_page]" type="number"
               value="<?php echo (isset($sets['rows_per_page']) ? $sets['rows_per_page'] : ''); ?>"
               min="0" max="100" >
        <label for="rows-per-page">Количество строк в таблице файлов и категорий.</label>
      <hr></p>

			<p><input type="submit" name="submit_settings" class="button button-primary" value="Сохранить"></p>
		</form>
    <br>
    <br>
    <p>Отладка.</p>
    <pre><?php
      echo '[settings_downloads] > '.htmlspecialchars(print_r($this->options, true));
      ?></pre>
    <p>Ручное управление базой данных.</p>
    <p><code><?php echo SITEURL; ?>?install</code> - создать таблицы базы, записать в них настройки, создать каталог для файлов</p>
    <p><code><?php echo SITEURL; ?>?uninstall</code> - удалить всю базу, настройки и файлы</p>
		<?php
	}

  function tab_description() { ?>
    <p>Опишу здесь кратко основные моменты приложения.</p>
    <br/>

    <p>Разрабатывал в браузере Iron, но всёбудет работать и в других браузерах Chromium таких как Google Crome, Яндекс браузер,
    Опера после 12 версии. В Firefox 23 тоже всё работает, остальные модерновые браузеры тоже должны всё правильно отображать.
    В Internet Explorer лучше не смотреть - задачей не было написать кроссбраузерно.</p>

    <p>Файлы загружаются в свою папку на сервере, данные о них такие как имя, mime тип и другие записываются в БД.</p>

    <p>Всё сделано с применением AJAX, за исключением станицы настроек которая обновляется обычным методом POST.</p>

    <br/>

    <p><b>Админка</b></p>

    <p><i>1. Вкладка "Загрузить"</i>. </p>

    <p>Здесь загружаются файлы на сервер, производится проверка на доступные расширения файлов, то есть какие можно загружать файлы.</p>

    <br/>

    <p><i>2. Вкладка "Файлы"</i>.</p>

    <p>Здесь отображается список загруженных файлов в таблице. Здесь я уделил внимание  горячим клавишам,
      так как с ними работа происходит быстрее. Нажав ссылку "Переименовать" откроется текстовое поле для переименования. Далее написав имя
      можно нажать ссылку "Сохранить", но можно нажать и "Enter" или "Tab" в случае с Табом сработает сохранение имени
      и тут же откроется поле переименования следующего файла, и так по порядку. По достижении конца, переименнование начнётся с начала.
      При нажатой клавише "Shift + Tab" поток переименования файлов пойдёт не вниз а вверх. Производится проверка на пустые строки
      и строки с пробелами то есть они отбрасываются.</p>

    <p>Нажатие на имя файла запустит процесс его скачивания.
      Кнопка "Разрешено/Запрещено" означает запрет скачивания. При попытке скачивания такого файла отобразится страница с
      соответствующим уведомлением. И такие файлы не отображаются на публичной странице равно как туда и не попадают файлы находящиеся в категории где запрещено скачивать. Казалось бы где смысл, ведь если файлов нет в паблике, то их нельзя и скачать?
      Это сделано для того, чтобы запретить скачивание уже опубликованных файлов, ссылки на которые уже известны - такую ссылку можно ввести в адресную строку браузера и процесс запустится, но этого не будет если доступ запрещён.</p>

    <p>Работа с категориями состоит из выпадающего списка к которому относятся 3 кнопки
      <ul><li> - "Фильтр" - отфильтровать отображение файлов по категории,</li>
      <li> - "Из" - Убрать из категории, </li>
      <li> - "В" - Добавить в категорию. </li></ul>
      Эти кнопки работают с файлами которые были выбраны помощью чекбоксов в самом левом столбце (работает технология Selecting помогающая выбирать файлы движениями мыши по строкам таблицы? здесь так же поддерживается клавиша "Shift").</p>

    <p>Вокруг списка категорий работают так же ссылки пагинации (навигации по страницам) и сортировка по расширению файла (нажатие на иконку расширения рядом с именем файла). Это означает, что при нажатии на ссылки пагинации и иконки расширений, они "смотрят" какая категория сейчас активна и работают в соответствии с этим, то есть навигация будет работать по файлам данной категории, и файлы с данным расширением будут выбираться из этой категории. Ссылки пагинации так же "смотрят" была ли выбрана сортировка по расширению,
      что даёт возможность перемещаться в выбранной категории по файлам конкретного расширения.</p>

    <p>Есть выпадающий список групповых действий позволяющий удалять и изменять права доступа нескольких файлов одновременно.</p>

    <p>Работает поиск по файлам.</p>

    <br>
    <p><b>Публичный доступ</b></p>

    <p>В публичном доступе остаётся только страница файлов с ограниченным функционалом - работает только фильтр по категориям, расширениям,
      пагинация.</p>


  <?php
  }

	function media_upload_form() {
		?>
		<div class="av-uploads-form">
			<?php

			if( ! device_can_upload()) {
				echo '<p>Браузер на вашем устройстве не поддерживает загрузку файлов.</p>';
				return;
			}

			$max_upload_size = max_upload_size();
			$allowed_size = get_size($max_upload_size);
			$nonce = create_nonce('media_form_upload');

			$plupload_init = array(
				'runtimes' => 'html5,html4,flash',
				'browse_button' => 'plupload-browse-button',
				'container' => 'plupload-upload-ui',
				'drop_element' => 'drag-drop-area',
				'file_data_name' => 'ajax-upload-file',
				'url' => SITEURL.'?ajax=1',
//				'headers' => array('Origin' => "http://host.ru"),
				'filters' => array(
					'mime_types' => array(
						array('title' => 'Допустимые расширения', 'extensions' => join(',', $this->allowed_ext))
					),
					'max_file_size' => $max_upload_size.'b',
				),
				'multipart_params' => array('_ajax_nonce' => $nonce, 'action' => 'upload')
			);
			?>
			<script type="text/javascript">
				var uploaderInitParams = <?php echo json_encode($plupload_init); ?>;
				var isMobile = <?php echo is_mobile() ? 1 : 0; ?>;
			</script>
			<div class="av-notice-bad"></div>
			<div class="av-notice-well"></div>
			<div id="plupload-upload-ui" class="hide-if-no-js">
				<div id="drag-drop-area">
					<div class="drag-drop-inside">
						<p class="drag-drop-info">Перетащите файлы сюда</p>
						<p>или</p>
						<p class="drag-drop-buttons">
							<input id="plupload-browse-button" type="button" value="Выберите файлы" class="button" style="position: relative; z-index: 0;">
						</p>
					</div>
				</div>
			</div>
			<p><span>Максимальный размер файла: <?php echo $allowed_size; ?></span></p>
			<div id="media-items" class="hide-if-no-js"></div>
			<p><input type="button" id="plupload-clean" name="clean" disabled class="button" value="Очистить">&nbsp;&nbsp;
				<input type="button" id="plupload-submit" class="button button-primary" value="Загрузить"></p>
		</div> <!-- /.av-uploads-form -->
	<?php
	}
}

