<?php defined('ABSPATH') or exit(''); ?>
<!doctype html>
<html lang="ru">
<head>
	<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Docs.cs</title>
	<link rel="stylesheet" type="text/css" href="<?php echo URL_CSS; ?>/styles.css" />
	<script src="<?php echo URL_SCRIPTS; ?>/jquery-1.10.2.js"></script>
  <script src="<?php echo URL_SCRIPTS; ?>/plupload.full.min.js"></script>
  <script src="<?php echo URL_SCRIPTS; ?>/handlers.js"></script>
  <script src="<?php echo URL_SCRIPTS; ?>/page.js"></script>
	<script src="<?php echo URL_SCRIPTS; ?>/jquery.ui.core.min.js"></script>
	<script src="<?php echo URL_SCRIPTS; ?>/jquery.ui.widget.min.js"></script>
	<script src="<?php echo URL_SCRIPTS; ?>/jquery.ui.mouse.min.js"></script>
	<script src="<?php echo URL_SCRIPTS; ?>/jquery.ui.selectable.min.js"></script>
</head>
<body>




