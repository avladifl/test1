function avdownPage($) {

	// LIBRARY
	function parseQuery(query, variable) {
		var vars = query.split("&");
		for(var i = 0; i < vars.length; i++) {
			var pair = vars[i].split("=");
			if(pair[0] == variable)
				return pair[1];
		}
		return false;
	}

	var pluralElem = ['элемент', 'элемента', 'элементов'];
	function plural(n) {
		return ((n==1) || (n%10==1 && n%100!=11)) ? 0 : ((n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20)) ? 1 : 2);
	}

  function globalState(target, data) {
    var $target = typeof target == 'string' ? $(target) : target;
    data = data || {};

    if( ! data.paged)
      data.paged = parseInt($('input[name=paged]', $target).val()) || '1';

    switch($target.attr('data-target')) {
      case 'uploaded':
        if( ! data.cat)
          data.cat = $('#cat option:selected', $target).val();
      //case 'categories':
    }

    return data;
  }

	function ajax(data, success, dataType, method) {
    dataType = dataType || 'json';
		$.ajax({
			url: ajaxurl,
			success: function(rs) {
        if(dataType == 'json') {
          if(typeof rs == 'string')
            console.log('avdown plugin error', JSON.stringify(rs));
          else if( ! rs.success)
            console.log('avdown plugin error', rs);
        }

        if(typeof success == 'function')
          success(rs);
      },
			type: method || 'POST',
			data: data || {},
			dataType: dataType,
			error: function(rs) {
        console.log('server error', rs);
      }
		});
	}
  // END LIBRARY

  // required data.tab; data.action
  function refreshTable(data, requestData) {
    var $tab = data.tab;

    requestData = requestData || {};
    requestData._ajax_nonce = $tab.find('input#_ajax_nonce').val();
    requestData.action = data.action;

    $tab.wrapInner('<div class="wrapAll" />');

    $tab.find('.wrapAll')
      .css('opacity', '0.6');

    var $shadow = $('.wrap .shadow');
    $shadow.css('display', 'block');

    $tab.append($shadow);

    boolAction = true;

    ajax(requestData, function(rs) {
      $('.wrap').append($shadow);
      $shadow.css('display', '');
      $tab.html(rs);

      if(data.callbackAjaxActions && typeof data.callbackAjaxActions == 'function')
        data.callbackAjaxActions();

      boolAction = false;
    }, 'html');
  }

  // TAB ACTIONS
	var tabActions = {
		upload: function() {},

		uploaded: function($link) {
			if($link.hasClass('refresh-table')) {
        $link.removeClass('refresh-table');
        refreshTable({
          tab: $('.uploaded'),
          action: 'update_table',
          callbackAjaxActions: ajaxActionsUploaded
        }, globalState('.uploaded'));
      }
		},

		categories: function($link) {
      if($link.hasClass('refresh-table')) {
        $link.removeClass('refresh-table');
        refreshTable({
          tab: $('.categories'),
          action: 'update_cats_table',
          callbackAjaxActions: ajaxActionsCategories
        }, globalState('.categories'));
      }
    },

		settings: function() {}
	};

	// click by tabs
  var boolAction = false;
  if($('#av-tabs a').length > 1) {
    $('#av-tabs a').click(function(e) {
      e.preventDefault();

      if(boolAction)
        return;

      var $newLink = $(this),
        newTab = $newLink.attr('id').replace('-tab', ''),
        $oldLink = $('#av-tabs a.nav-tab-active:first'),
        oldTab = $oldLink.attr('id').replace('-tab', '');

      if(newTab == oldTab)
        return;

      var $oldScreen = $('.av-tab-screen.'+ oldTab),
        $newScreen = $('.av-tab-screen.'+ newTab);

      window.location.hash = newTab;
      setReferrerHash(newTab);

      $oldScreen.animate({opacity: 0}, 100, 'linear', function(an, prg) {
        $oldScreen.css({ display: 'none' });
        $oldLink.removeClass('nav-tab-active');
        $newScreen.css('display', 'block');
        $newScreen.css('opacity', 0);
        $newScreen.animate({opacity: 1}, 100, 'linear', function() {
          $newLink.addClass('nav-tab-active');
        });
      });

      // tabs actions
      tabTarget = newTab;
      if(typeof tabActions[newTab] == 'function') {
        tabActions[newTab]($newLink);
      }
    });
  }

  $('.av-tab-screen:first').css('display', 'block');
	// tabs
  function setReferrerHash(newTab) {
    $('.av-tab-screen form input[type="hidden"][name="_http_referer"]')
      .each(function(i, item) {
        $item = $(item);
        var value = $item.attr('value').replace(/#.+$/, '');
        $item.attr('value', value +'#'+ newTab);
      });
  }

	// tabs
  var action = window.location.hash.replace('#', '');
  var $tabs = $('#av-tabs a.nav-tab');
  var firstLink = $tabs.filter(':first').attr('id').replace('-tab', '');
  var isTab = $tabs.is('#' + action + '-tab');
  if(action && isTab && action != firstLink) {
    $('.av-tab-screen').css('display', 'none');
    $('.nav-tab').removeClass('nav-tab-active');
    $('#' + action + '-tab').addClass('nav-tab-active');
    $('div.' + action).css('display', 'block');
    setReferrerHash(action);
  }
  // END TAB ACTIONS


  function tablenavPages(e) {
    e.preventDefault();

    if($(this).is('.disabled'))
      return;

    var query = this.search.substring(1);
    
    var requestData = {};
        requestData.paged = parseQuery(query, 'paged') || '1';

    var $ext = $('input[name=ext]');
    if($ext.length == 1)
      requestData.ext = $ext.val();

    requestData = globalState(e.data.tab, requestData);

    refreshTable(e.data, requestData);
  }

  var timer;
  function paged(e) {
    if(13 == e.which)
      e.preventDefault();

    var requestData = {
      paged: parseInt($('input[name=paged]', e.data.tab).val()) || '1'
    };

    requestData = globalState(e.data.tab, requestData);
    
    window.clearTimeout(timer);
    timer = window.setTimeout(function() {
      refreshTable(e.data, requestData);
    }, 500);
  }


// RENAMING ITEM
  var globalRename = null;
  function renameAction(e) {
    e.preventDefault();
    var $tr = $(this).closest('tr');
    $tr
      .removeClass('rename-row-bad')
      .addClass('rename-row')
      .find('.td_name .rename-bad')
      .remove();

    $tr.find('.field-edit-action').focus().select();

    if(globalRename && globalRename != e.target)
      $(globalRename).closest('tr').removeClass('rename-row');

    globalRename = this;
  }

  document.addEventListener('click', function (e) {
    var $targ = $(e.target);

    if($targ.is('.field-edit-action'))
      return;

    if($targ.is('.save-rename-action'))
      return;

    if(globalRename && globalRename != e.target)
      $(globalRename).closest('tr').removeClass('rename-row');
  }, true);

  var renaming = tab = shift = false;
  function saveRenameAction(e) {
    e.preventDefault();

    var $tr = $(this).closest('tr');
    var $idinput = $tr.find('.id-item');
    var id = parseInt($idinput.val(), 10);
    var name = $('.name'+id, $tr).text();
    var newName = $('.new-name'+id, $tr).val();

    if(name == newName || newName == '' || newName.search(/^\s+$/) == 0) {
      $tr.removeClass('rename-row');
      $('.new-name'+id, $tr).val(name);
      if(tab) tabPress($tr);
      return;
    }

    if(renaming)
      return;

    renaming = true;
    boolAction = true;

    $tr
      .find('.td_name')
      .append($('div .spinner-small:first').clone().attr({height: '15', width: '15'}));

    var requestData ={
      action: e.data.action,
      new_name: newName,
      _ajax_nonce: $idinput.attr('data-nonce'),
      id: id
    };


    ajax(requestData, function(rs) {
      if(rs.success) {
        $('.name'+id, $tr).text(rs.data.new_name);
        $('.new-name'+id, $tr).val(rs.data.new_name);
        $idinput.attr('data-nonce', rs.data.nonce);
      }
      else {
        $('.name'+id, $tr)
          .text(name)
          .before('<span class="rename-bad">'+ (rs.error || 'Неудача.') +'</span>');
        $tr.addClass('rename-row-bad');
      }

      $tr.find('.td_name .spinner-small').remove();
      $tr.removeClass('rename-row');

      if(tab) tabPress($tr);

      if(e.data) {
        if(e.data.afterRename && typeof e.data.afterRename == 'function')
          e.data.afterRename();
      }

      renaming = false;
      boolAction = false;
    });
  }

  function tabPress($tr) {
    var $second = $tr[shift ? 'prev' : 'next']();

    if($second.is('.area-edit-row'))
      $second = $second[shift ? 'prev' : 'next'](':not(.area-edit-row)');

    if($second.length == 0)
      $second = $tr.parent().find('tr:not(.area-edit-row):'+ (shift ? 'last' : 'first'));

    if($second.is('tr'))
      $second.find('.rename-action').trigger('click');

    tab = false;
  }

  function fieldEditAction(e) {
    shift = e.shiftKey;

    if(renaming)
      return;

    switch(e.which) {
      case 9:
        tab = true;
      case 13:
        e.preventDefault();
        $(this).closest('td').find('.save-rename-action').trigger('click');
    }
  }
  // END RENAMING ITEM

  function allCheckboxes(data) {
    var $target = data.tab;
    var $allCheckboxes = $('tbody, thead, tfoot', $target)
      .find('.check-column :checkbox');

    $allCheckboxes
      .unbind('click');

    var $edgeCheckboxes = $('thead, tfoot', $target)
      .find('.check-column :checkbox');

     $edgeCheckboxes.each(function(i, elem) {
      $(elem).click(function(e) {
        var checked = $(this).prop('checked');
        $allCheckboxes.prop('checked', checked);
        $('.av-selectable > tr', $target)[(checked ? 'add' : 'remove') + 'Class']('ui-selected');
      });
    });

    $('div.bulkactions #doaction, div.bulkactions #doaction2', $target).click(function(e) {
      e.preventDefault();
      var $checkboxes = $('.check-column[scope="row"] input[type="checkbox"]:checked', $target);

      if($checkboxes.length <= 0)
        return false;

      var $bulkActions = $(this).closest('div.bulkactions');
      var $option = $('option:selected', $bulkActions);

      var actions = {
        'perm-yes': 'permission',
        'perm-no': 'permission',
        'delete': 'delete'
      };
      var val = $option.val();

      if(val <= 0)
        return false;

      $buttons = $checkboxes
        .closest('tr')
        .find('input.' + actions[val]);

      var requestData = { items: [] };

      $buttons.each(function (i, elem) {
        if(actions[val] == 'permission') {
          if($(elem).is(val == 'perm-yes' ? '.yes' : '.no'))
            return;
        }
        requestData.items.push($(elem).closest('tr').find('.id-item').val());
      });

      if(requestData.items.length == 0)
        return;

      data.action += actions[val];

      if(actions[val] == 'permission')
        requestData.swtch = val;

      requestData = globalState($target, requestData);

      var $ext = $('input[name=ext]');
      if($ext.length == 1)
        requestData.ext = $ext.val();

      refreshTable(data, requestData);
    });
  }
    
  var processPermDel = false;
  function buttonPermDel(data, action, callback) {
    $tab = data.tab;

    if(processPermDel)
      return;

    processPermDel = true;

    var $target = $(this);
    var $tr = $target.closest('tr');
    var $idinput = $tr.find('.id-item');
    var isDel = $target.is('.delete');

    $target
      .addClass('loading-button');

    var requestData = {
      action: action,
      _ajax_nonce: $idinput.attr('data-nonce'),
      id: $idinput.val()
    };

    ajax(requestData, function(rs) {
      if(isDel && rs.success) { // if delete action
        $tbody = $tr.parent();
        $tr.fadeOut(200, function() {
          $tr.remove();

          var $display = $('.displaying-num', $tab);
          var num = parseInt($display.html().split(' ')[0], 10) - 1;
          $display.html(num +' '+ pluralElem[plural(num)]);

          $rows = $tbody.find('> tr');
          if($rows.length != 0) {
            $rows
              .removeClass('alternate')
              .filter(':even')
              .addClass('alternate');
          }
          else {
            $('.tablenav.top .next-page, .tablenav.top .prev-page', $tab)
              .filter(':not(.disabled)')
              .trigger('click');
          }
        });
      }
      else {
        if(rs.success) { // if perm_down action
          $target
            .removeClass('loading-button')
            .val(!!rs.data.perm ? 'Разрешено' : 'Запрещено')
            .removeClass('yes no')
            .addClass(!!rs.data.perm ? 'yes' : 'no');

          $idinput.attr('data-nonce', rs.data.nonce);
        }
      }

      processPermDel = false;

      if(data.afterDelete && typeof data.afterDelete == 'function')
        data.afterDelete();

      if(typeof callback == 'function')
        callback();
    });
  }

  function extend(obj, add) {
    var newObj = {};
    for(name in obj)
      newObj[name] = obj[name];

    for(var name2 in add)
      newObj[name2] = add[name2];

    return newObj;
  }

  // ajax actions
  function ajaxActionsUploaded() {
    var data = {
      tab: $('.uploaded'),
      callbackAjaxActions: ajaxActionsUploaded
    };

    $('.uploaded .tablenav-pages a').bind('click', extend(data, { action: 'update_table' }), tablenavPages);
    $('.uploaded input[name=paged]').bind('keyup', extend(data, { action: 'update_table' }), paged);
    $('.uploaded .rename-action').bind('click', data, renameAction);
    $('.uploaded .save-rename-action').bind('click', { action: 'rename_file' }, saveRenameAction);
    $('.uploaded input.field-edit-action').bind('keydown', fieldEditAction);
    $('.uploaded input.permission').bind('click', function(e, callback) { // callback for trigger
      buttonPermDel.call(this, data, 'perm_file', callback);
    });
    $('.uploaded input.delete').bind('click', function(e, callback) { // callback for trigger
      var dt = extend(data, {
        afterDelete: function() { $('#categories-tab').addClass('refresh-table'); }
      });

      buttonPermDel.call(this, dt, 'delete_file', callback);
    });

    $('.uploaded #categories-filter, .uploaded #to-category, .uploaded #from-category').bind('click', function(e) {
      e.preventDefault();
      var $button = $(this);
      var action = $button.attr('name');
      var val = $button.closest('div.actions').find('#cat option:selected').val();

      if(val <= 0 && $button[0].id == 'to-category')
        return false;

      var requestData = {
        cat: val,
        submit: action
      };

      requestData = globalState(data.tab, requestData);

      var $checkboxes = $('.uploaded .check-column[scope="row"] input[type="checkbox"]:checked');

      if(action == 'to-cat' || action == 'from-cat') {
        if($checkboxes.length <= 0)
          return false;

        requestData.files = [];
        $checkboxes.each(function(i, elem) {
          requestData.files.push(elem.value);
        });
      }

      var actions = {
        'to-cat': 'to_category',
        'from-cat': 'from_category',
        'cat-filter': 'update_table'
      };

      if($button[0].id != 'categories-filter')
        $('#categories-tab').addClass('refresh-table');

      refreshTable(extend(data, { action: actions[action] }), requestData);
    });

    $('.edit-action').bind('click', function(e) {
      window.location.href = this.href;
    });

    $('.sort-ext').bind('click', function(e) {
      var $this = $(this);
      var ext = $this.attr('data-sort');
      refreshTable(
        extend(data, { action: 'update_table' }),
        extend({ ext: ext }, globalState('.uploaded'))
      );
    });

    $('#all-categories', data.tab).bind('click', function(e) {
      var $this = $(this);
      $('.postform option[value=0]', data.tab)
        .prop('selected', true);
      $('#categories-filter', data.tab).trigger('click');
    });

    launchSearch(data, 'update_table', 'search_files');

    setTimeout(function() {
      allCheckboxes(extend(data, {
        callbackAjaxActions: function() {
          ajaxActionsUploaded();
          $('#categories-tab').addClass('refresh-table');
        },
        action: 'uploaded_group_'
      }));
      avselectable(data.tab);
    }, 0);
  }
  ajaxActionsUploaded();

  function ajaxActionsCategories() {
    var data = {
      tab: $('.categories'),
      callbackAjaxActions: ajaxActionsCategories
    };

    $('.categories .tablenav-pages a').bind('click', extend(data, { action: 'update_cats_table' }), tablenavPages);
    $('.categories input[name=paged]').bind('keyup', extend(data, { action: 'update_cats_table' }), paged);
    $('.categories .rename-action').bind('click', data, renameAction);

    $('.categories .save-rename-action').bind('click', extend(data, { action: 'rename_cat' }), function(e) {
      e.data.afterRename = function() { $('#uploaded-tab').addClass('refresh-table'); };
      saveRenameAction.call(this, e);
    });

    $('.categories input.field-edit-action').bind('keydown', fieldEditAction);
    $('.categories input.permission').bind('click', function(e, callback) { // callback for trigger
      buttonPermDel.call(this, data, 'perm_cat', callback);
    });
    $('.categories input.delete').bind('click', function(e, callback) { // callback for trigger
      var dt = extend(data, {
        afterDelete: function() { $('#uploaded-tab').addClass('refresh-table'); }
      });

      buttonPermDel.call(this, dt, 'delete_cat', callback);
    });

    $('.categories #newcat').bind('click', newcat);
    $('.categories #newcatInput').bind('keydown', function(e) {
      if(e.which == 13) {
        newcat(e);
      }
    });

    function newcat(e) {
      e.preventDefault();

      var newcat = $('#newcatInput').val();

      if( ! newcat)
        return;

      var requestData = {
        cat: newcat,
        submit: 'newcat'
      };

      var $lastPage = $('.tablenav-pages:first a.last-page', data.tab);
      var lastPage = parseQuery($lastPage[0].search.substring(1), 'paged') || '1';
      requestData.paged = lastPage + 1;

      refreshTable(extend(data, {
        callbackAjaxActions: function() {
          ajaxActionsCategories();
          $('#newcatInput').focus();
          $('#uploaded-tab').addClass('refresh-table');
        },
        action: 'new_category'
      }), requestData);
    }

    launchSearch(data, 'update_cats_table', 'search_cats');

    setTimeout(function() {
      allCheckboxes(extend(data, {
        callbackAjaxActions: function() {
          ajaxActionsCategories();
          $('#uploaded-tab').addClass('refresh-table');
        },
        action: 'categories_group_'
      }));
      avselectable(data.tab);
    }, 0);
  }
  ajaxActionsCategories();

  function avselectable($target) {
    if($target.is('.public'))
      return;
    
    $('.av-selectable', $target).selectable({
      filter: 'tr',
      selecting: function(event, ui) {
        var $tr = $(ui.selecting);
        $tr.find('input.id-item').prop('checked', true);
      },
      unselecting: function(event, ui) {
        var $tr = $(ui.unselecting);
        $tr.find('input.id-item').prop('checked', false);
      }
    });

    $('.id-item', $target).bind('click', function(e) {
      var $this = $(this);
      var checked = $this.prop('checked');
      $this.closest('tr')[(checked ? 'add' : 'remove') + 'Class']('ui-selected');
    });
  }

  function launchSearch(data, action, name) {
    $('.search-submit', data.tab).bind('click', function(e) {
      e.preventDefault();
      var $search = $('input[name='+ name +']');
      var searchText = $search.val();

      if(searchText == '' || searchText.search(/^\s+$/) == 0) {
        return;
      }

      var requestData = {};
          requestData[name] = searchText;

      var tempCallbackAjaxActions = data.callbackAjaxActions;
      refreshTable(extend(data, {
        callbackAjaxActions: function() {
          tempCallbackAjaxActions();
          console.log(tempCallbackAjaxActions);
          $('input[name='+ name +']').focus();
        },
        action: action
      }), requestData);
    });
  }
}