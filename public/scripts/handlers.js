;(function($) {
	var messages = {
    default_error: 'Во время загрузки произошла ошибка. Пожалуйста, повторите попытку позже.',
    error_uploading: 'Файл <b>"%s"</b> загрузить не удалось.',
    file_exceeds_size_limit: 'Размер файла <b>"%s"</b> превышает максимальный для этого сайта.',
    http_error: 'Ошибка HTTP.',
    invalid_filetype: 'Файл не принадлежит к разрешённым типам.',
    io_error: 'Ошибка ввода/вывода.',
    security_error: 'Ошибка безопасности.',
    upload_failed: 'Загрузка не удалась.'
	};

	function item(up, file) {
		var $item = $('#'+ file.id);
		if($item.length == 0) {
			$item = $('#media-items')
				.append('<div id="'+ file.id +'" class="media-item"><div class="message">'+ file.name +'</div><a class="dismiss remove-from-plupload" href="#">Удалить из списка</a></div>')
				.find('#'+ file.id);
		}
		return $item;
	}

	function eFilesAdded(up, file) {
		$('.av-notice-well, .av-notice-bad').html('');
		plupload.each(up.files, function(file) {
			item(up, file);
		});
	}

	function fileError(up, file, message) {
		var $item = item(up, file),
				filename = $item.find('.message').text();

		$item.removeClass('well bad');
		$item.addClass('bad');
		$item.html('<div class="message">'+ messages.error_uploading.replace('%s', jQuery.trim(filename)) +' '+ message +'</div>'+
								'<a class="dismiss close-item" href="#">Закрыть</a>');
	}

	function fileExtensionError(up, file, message) {
		var $item = item(up, file);
		$item.addClass('bad');
		$item.html('<div class="message">Файл <b>"'+ file.name +'"</b> не принадлежит к разрешённым типам.</div>'+
								'<a class="dismiss close-item" href="#">Закрыть</a>');
	}

	function uploadSizeError(up, file) {
		var $item = item(up, file);
		$item.addClass('bad');
		$item.html('<div class="message">'+ messages.file_exceeds_size_limit.replace('%s', file.name) +'</div>'+
								'<a class="dismiss close-item" href="#">Закрыть</a>');
	}

	function avError(message) {
		$('.av-notice-well, .av-notice-bad').html('');
		$('.av-notice-bad').html('<p>'+ message +'</p>');
	}

	function eUploadProgress(up, file) {
		var $item = item(up, file);
		var html =  '<div class="progress">'+
									'<div class="percent">'+ file.percent +'%</div>'+
									'<div class="bar" style="width: '+ (file.percent / 100 * 200) +'px;"></div>'+
								'</div>'+
								'<div class="message">'+ file.name +'</div>';
		$item.html(html);
	}

	function eFileUploaded(up, file, serverData) {
		var $item = item(up, file);
				$item.removeClass('bad well');

    try {
      var response = JSON.parse(serverData.response);
    }
    catch(err) {
      var rsp = serverData.response;
      var str = '';
      if(rsp.match(/Content-Length.+(\d+).+(\d+) bytes/ig)) {
        var rg = /\d+/g,
            fileSize = rg.exec(rsp),
            uploadSize = rg.exec(rsp);
        str = messages.file_exceeds_size_limit.replace('%s', file.name);
      }

      $item
        .addClass('bad')
        .html('<div class="message">Ошибка сервера.</div><div class="content-message">'+ (str || serverData.response) +'</div>');
      return;
    }

		if(response.success == true && serverData.status == 200) {
			var message = '';
			if(typeof response.data == 'string')
      	message = response.data;
			else {
				message = response.data.message;
			}

			$item
				.addClass('well')
				.html('<div class="message">'+ message +'</div>');
		}
    else {
      var error = response.error || serverData.response;
      $item
        .addClass('bad')
        .html('<div class="message">Ошибка.</div><div class="content-message">'+ error +'</div>');
        /* .html('<div class="message">Ошибка сервера. Статус: '+ serverData.status +'.</div><div class="content-message">'+ error +'</div>'); */
    }
	}

  function eUploadComplete() {
    $('#uploaded-tab')
      .addClass('refresh-table');
  }

	function eError(up, err) {
		var file = err.file;

		switch (err.code) {
			case plupload.FAILED:
				fileError(up, file, messages.upload_failed);
				break;
			case plupload.FILE_EXTENSION_ERROR:
				fileExtensionError(up, file, messages.invalid_filetype);
				break;
			case plupload.FILE_SIZE_ERROR:
				uploadSizeError(up, file);
				break;
			case plupload.GENERIC_ERROR:
				fileError(up, file, messages.upload_failed);
				avError(messages.upload_failed);
				break;
			case plupload.IO_ERROR:
				fileError(up, file, messages.io_error);
				avError(messages.upload_failed);
				break;
			case plupload.HTTP_ERROR:
				fileError(up, file, messages.http_error);
				avError(messages.http_error);
				break;
			case plupload.SECURITY_ERROR:
				avError(messages.security_error);
				break;
			default:
				fileError(up, file, messages.default_error);
		}

		up.removeFile(file);
		up.refresh();

		if(uploader.state != plupload.STARTED)
			$('input#plupload-clean').prop('disabled', false);
	}

	$(document).ready(function($) {

		$('.av-uploads-form').click(function(e) {
			var $target = $(e.target),
					$hideElem;

			if($target.is('a.dismiss')) {
				e.preventDefault();
				$hideElem = $target.parents('.media-item');
				uploader.removeFile($hideElem[0].id);
			} 
			else if($target.is('input#plupload-clean')) {
//				e.preventDefault();
//				$hideElem = $target.parents('.media-item');
//        uploader.removeFile($hideElem[0].id);
			} 

			if($hideElem) {
				$hideElem.fadeOut(200, function(){
					$(this).remove();
				});
			}
		});

    uploaderInit = function() {
      uploader = new plupload.Uploader(uploaderInitParams);

      uploader.bind('Init', function(up) {
        var $uploaddiv = $('#plupload-upload-ui');
        if(up.runtime === 'html4' || ! uploader.features.dragdrop || isMobile) {
          $uploaddiv.removeClass('drag-drop');
          $('#drag-drop-area').unbind('.av-uploader');
        }
        else {
          $uploaddiv.addClass('drag-drop');
          $('#drag-drop-area')
            .bind('dragover.av-uploader', function(){ // dragenter doesn't fire right :(
              $uploaddiv.addClass('drag-over');
            })
            .bind('dragleave.av-uploader, drop.av-uploader', function(){
              $uploaddiv.removeClass('drag-over');
            });
        }
      });

			uploader.init();

      uploader.bind('PostInit', function(up) {
        var $ui = $('#plupload-upload-ui');
        var $uiLastDiv = $(' > div:last', $ui);
        var $browseButton = $('#plupload-browse-button');
        var uiLastDivRect = $uiLastDiv[0].getBoundingClientRect();

        if(    $uiLastDiv.hasClass('moxie-shim')
            && $ui.hasClass('drag-drop')
            && $uiLastDiv.css('top') == '0px'
            && $uiLastDiv.css('left') == '0px') {

          function positionButtonForBadBrowsers() { // for Iron and etc...
            var pluploadUIRect = $ui[0].getBoundingClientRect();
            //var divPluploadRect = $uiLastDiv[0].getBoundingClientRect();
            var inputRect = $browseButton[0].getBoundingClientRect();

            $uiLastDiv
              .css({
                left: (inputRect.left - pluploadUIRect.left) +'px',
                top: (inputRect.top - pluploadUIRect.top) +'px',
                height: inputRect.height +'px',
                width: inputRect.width +'px'
              })
              .find('input[type="file"]')
              .css({
                height: inputRect.height +'px',
                width: inputRect.width +'px'
              });
          }
          $browseButton.bind('mousemove', positionButtonForBadBrowsers);
          positionButtonForBadBrowsers();
        }
      });

			var $clean = $('input#plupload-clean');
			$clean
				.prop('disabled', false)
				.click(function(e) {
					if(uploader.state == plupload.STARTED)
						return;

					$('.media-item').each(function(j, elem) {
						uploader.removeFile(elem.id);
						$(elem).fadeOut(200, function() {
							$(elem).remove();
						});
					});

					$clean.prop('disabled', true);
				});

      uploader.bind('FilesAdded', function(up, files) {
        //console.log('FilesAdded');
        eFilesAdded(up, files);

        $clean.prop('disabled', false);

        plupload.each(files, function(file) {
          if(uploader.state == plupload.STARTED)
            eUploadProgress(up, file);
        });
      });

      uploader.bind('UploadFile', function(up, file) { // fires before uploading of file
        //console.log('UploadFile');
        // eUploadFile(up, file);
      });

      uploader.bind('UploadProgress', function(up, file) {
        //console.log('UploadProgress');
        eUploadProgress(up, file);
      });

      uploader.bind('Error', function(up, err) {
        //console.log('Error');
        eError(up, err);
      });

      uploader.bind('FileUploaded', function(up, file, response) {
        //console.log('FileUploaded');
        eFileUploaded(up, file, response);
      });

      uploader.bind('UploadComplete', function() {
        //console.log('UploadComplete');
        eUploadComplete();
      });
		};

		$('#plupload-submit').click(function(e) {
			uploader.start();
			plupload.each(uploader.files, function(file) {
			  // DONE: 5; FAILED: 4; UPLOADING: 2; QUEUED: 1;
				if(file.status < 3)
					eUploadProgress(uploader, file);
			});

		});

		if(typeof(uploaderInitParams) == 'object') {
			uploaderInit();
    }
	});
}(jQuery));