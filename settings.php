<?php defined('ABSPATH') or exit('');

define('DB_NAME', 'docs_cs');
define('DB_USER', 'admin');
define('DB_PASSWORD', 'admin');
define('DB_HOST', 'localhost');
define('DB_CHARSET', 'utf8');
define('DB_COLLATE', '');

define('SITEURL', 'http://docs.cs/');
define('INCLUDES', ABSPATH.'includes');
define('UPLOADS_DIR', ABSPATH.'uploads');

define('PATH_ICONS', ABSPATH.'icons');
define('URL_ICONS', SITEURL.'icons');

define('PATH_IMGS', ABSPATH.'public');
define('URL_IMGS', SITEURL.'public');

define('URL_SCRIPTS', SITEURL.'public/scripts');
define('URL_CSS', SITEURL.'public');
